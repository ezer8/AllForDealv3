package welcome;

import edu.esprit.pidev.vues.Accueil;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Main extends JPanel implements ActionListener {

    public static JFrame app;
    public static Main anim;
    ImageIcon images[];
    int totalImages = 71, currentImage = 0, animationDelay = 30;
    Timer animationTimer;
    boolean end = false;

    public Main() {

        images = new ImageIcon[totalImages];
        for (int i = 0; i < images.length; ++i) {
            images[i] = new ImageIcon("C:\\Users\\Asus\\Documents\\NetBeansProjects\\AllForDealv3\\src\\welcome\\" + i + ".png");
        }
        startAnimation();

    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (images[currentImage].getImageLoadStatus() == MediaTracker.COMPLETE) {
            images[currentImage].paintIcon(this, g, 0, 0);
            currentImage++;
            if (currentImage == totalImages) {
                stopAnimation();
              
            }
        }
    }

    public void actionPerformed(ActionEvent e) {
        repaint();
    }

    public void startAnimation() {

        if (animationTimer == null) {
            currentImage = 0;
            animationTimer = new Timer(animationDelay, this);
            animationTimer.start();
        } else if (animationTimer.isRunning()) {
              
            animationTimer.restart();
        }

    }

    public void stopAnimation() {
        animationTimer.stop();
         app.dispose();
          new Accueil().startapp();
       
          
          
    }

    public static void main(String args[]) {

        Main anim = new Main();
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

        app = new JFrame("Animator test");
        app.setLocation((dim.width / 2 - app.getSize().width / 2) - 255, (dim.height / 2 - app.getSize().height / 2) - 255);
        app.add(anim, BorderLayout.CENTER);
        app.setSize(500, 500);
        app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        app.setUndecorated(true);
        app.setBackground(Color.yellow);
        //app.setSize(anim.getPreferredSize().width + 10, anim.getPreferredSize().height + 30);
        app.setVisible(true);

    }
}
