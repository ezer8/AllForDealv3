/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication3;

import java.io.IOException;
import javafx.animation.FadeTransition;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import javafx.scene.shape.*;
import javafx.animation.*;
import javafx.scene.control.Button;
import javafx.scene.transform.Rotate;
import javafx.stage.Modality;

/**
 *
 * @author Shamileth
 */
public class JavaFXApplication3 extends Application {

    @FXML
    Label c1;
   
    public static Stage stage;
   

    public static void main(String[] args) {
      
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        root.setStyle("-fx-background-color: transparent;");
        Scene scene = new Scene(root, Color.TRANSPARENT);
        scene.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
        primaryStage.initStyle(StageStyle.TRANSPARENT);
        
      primaryStage.initModality(Modality.APPLICATION_MODAL);
        primaryStage.setScene(scene);
        primaryStage.show();
        
        stage = primaryStage;
    }
    
    public void start2(){
    launch();
    }
    
    
   
}
