/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication3;

import displayshelf.Shelf;
import edu.esprit.pidev.vues.Accueil;
import static edu.esprit.pidev.vues.Accueil.scene;
import edu.esprit.pidev.vues.AccueilController;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.FadeTransition;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.util.Duration;
import javafx.geometry.*;
import javafx.scene.effect.*;
import javafx.scene.paint.*;
import javafx.scene.shape.*;
import java.lang.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Shamileth
 */
public class FXMLDocumentController implements Initializable {

    Timeline timelineb = new Timeline();
    @FXML
    private Button b;
    @FXML
    private Label c1;
    @FXML
    private Label c2;
    @FXML
    private Label c3;
    @FXML
    private Label c4;
    @FXML
    private Label c5;
    @FXML
    private Label c6;
    private boolean ok = false;
    @FXML
    private Label info;

    @FXML
    private void test(ActionEvent e) {

    closeM();
    }

    @FXML
    private void modifierprofil(MouseEvent e) {
        Timeline timeline = new Timeline();

        if (e.getSource() == c1) {
            info.setText("Modifier Profil");
            info.setVisible(true);
            KeyFrame key1 = new KeyFrame(Duration.millis(100), new KeyValue(info.opacityProperty(), 1));
            timeline.getKeyFrames().addAll(key1);
            timeline.play();

        } else if (e.getSource() == c2) {
            info.setText("Afficher Profil");
            info.setVisible(true);
            KeyFrame key1 = new KeyFrame(Duration.millis(100), new KeyValue(info.opacityProperty(), 1));
            timeline.getKeyFrames().addAll(key1);
            timeline.play();

        } else if (e.getSource() == c3) {
            info.setText("Notifications");
            info.setVisible(true);
            KeyFrame key1 = new KeyFrame(Duration.millis(100), new KeyValue(info.opacityProperty(), 1));
            timeline.getKeyFrames().addAll(key1);
            timeline.play();

        } else if (e.getSource() == c4) {
            info.setText("Messages");
            info.setVisible(true);
            KeyFrame key1 = new KeyFrame(Duration.millis(100), new KeyValue(info.opacityProperty(), 1));
            timeline.getKeyFrames().addAll(key1);
            timeline.play();
        } else if (e.getSource() == c5) {
            info.setText("Consulter Offres");
            info.setVisible(true);
            KeyFrame key1 = new KeyFrame(Duration.millis(100), new KeyValue(info.opacityProperty(), 1));
            timeline.getKeyFrames().addAll(key1);
            timeline.play();

        } else if (e.getSource() == c6) {
            info.setText("Deconnecter");
            info.setVisible(true);
            KeyFrame key1 = new KeyFrame(Duration.millis(100), new KeyValue(info.opacityProperty(), 1));
            timeline.getKeyFrames().addAll(key1);
            timeline.play();

        }

    }

    @FXML
    private void modifierprofil1(MouseEvent e) {
        Timeline timeline = new Timeline();

        info.setVisible(false);
        KeyFrame key1 = new KeyFrame(Duration.millis(100), new KeyValue(info.opacityProperty(), 0));
        timeline.getKeyFrames().addAll(key1);
        timeline.play();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO

        KeyFrame keyb = new KeyFrame(Duration.millis(500),
                new KeyValue(b.opacityProperty(), 1));
        timelineb.getKeyFrames().addAll(keyb);
        timelineb.play();

        timelineb.setOnFinished(e -> {openM();});
        c1.setOnMouseClicked(e->{closeM();
        Stage st = new Stage();
            try {
                Parent root2 = FXMLLoader.load(getClass().getResource("/edu/esprit/pidev/vues/modifProf.fxml"));
                 Scene scene = new Scene(root2);
                 st.setScene(scene);
                 st.show();
            } catch (IOException ex) {
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            }
           
        });
         c2.setOnMouseClicked(e->{closeM();
        Stage st = new Stage();
            try {
                Parent root2 = FXMLLoader.load(getClass().getResource("/edu/esprit/pidev/vues/ProfilDc.fxml"));
                 Scene scene = new Scene(root2);
                 st.setScene(scene);
                 st.show();
            } catch (IOException ex) {
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            }
           
        });
          c3.setOnMouseClicked(e->{closeM();
        Stage st = new Stage();
            try {
                Parent root2 = FXMLLoader.load(getClass().getResource("/edu/esprit/pidev/vues/modifProf.fxml"));
                 Scene scene = new Scene(root2);
                 st.setScene(scene);
                 st.show();
            } catch (IOException ex) {
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            }
           
        });
   
           c6.setOnMouseClicked(e->{closeM();
            try {
                Parent root2 = FXMLLoader.load(getClass().getResource("/edu/esprit/pidev/vues/Authentification.fxml"));
                 Scene scene = new Scene(root2);
                 Accueil.stage.setScene(scene);
                 Accueil.stage.show();
                 
            } catch (IOException ex) {
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            }
           
        });
    
    }
public void openM(){

        if (ok == false) {
            Timeline timeline = new Timeline();
            Timeline timeline1 = new Timeline();
            Timeline timeline2 = new Timeline();
            Timeline timeline3 = new Timeline();
            Timeline timeline4 = new Timeline();
            Timeline timeline5 = new Timeline();

            KeyFrame key = new KeyFrame(Duration.millis(100),
                    new KeyValue(c1.opacityProperty(), 1));
            KeyFrame key111 = new KeyFrame(Duration.millis(100), new KeyValue(c1.translateXProperty(), 120));
            KeyFrame key111r = new KeyFrame(Duration.millis(100), new KeyValue(c1.rotateProperty(), 0));

            KeyFrame key11 = new KeyFrame(Duration.millis(100), new KeyValue(c2.translateXProperty(), 115));
            KeyFrame key12 = new KeyFrame(Duration.millis(100), new KeyValue(c2.translateYProperty(), -220));
            KeyFrame key13 = new KeyFrame(Duration.millis(100), new KeyValue(c2.opacityProperty(), 1));
            KeyFrame key11r = new KeyFrame(Duration.millis(100), new KeyValue(c2.rotateProperty(), 0));

            KeyFrame key21 = new KeyFrame(Duration.millis(100), new KeyValue(c3.translateXProperty(), -115));
            KeyFrame key22 = new KeyFrame(Duration.millis(100), new KeyValue(c3.translateYProperty(), -220));
            KeyFrame key23 = new KeyFrame(Duration.millis(100), new KeyValue(c3.opacityProperty(), 1));
            KeyFrame key21r = new KeyFrame(Duration.millis(100), new KeyValue(c3.rotateProperty(), 0));

            KeyFrame key3 = new KeyFrame(Duration.millis(100), new KeyValue(c4.opacityProperty(), 1));
            KeyFrame key44 = new KeyFrame(Duration.millis(100), new KeyValue(c4.translateXProperty(), -100));
            KeyFrame key31r = new KeyFrame(Duration.millis(100), new KeyValue(c4.rotateProperty(), 0));

            KeyFrame key41 = new KeyFrame(Duration.millis(100), new KeyValue(c5.translateXProperty(), -110));
            KeyFrame key42 = new KeyFrame(Duration.millis(100), new KeyValue(c5.translateYProperty(), 210));
            KeyFrame key43 = new KeyFrame(Duration.millis(100), new KeyValue(c5.opacityProperty(), 1));
            KeyFrame key41r = new KeyFrame(Duration.millis(100), new KeyValue(c5.rotateProperty(), 0));

            KeyFrame key51 = new KeyFrame(Duration.millis(100), new KeyValue(c6.translateXProperty(), 110));
            KeyFrame key52 = new KeyFrame(Duration.millis(100), new KeyValue(c6.translateYProperty(), 210));
            KeyFrame key53 = new KeyFrame(Duration.millis(100), new KeyValue(c6.opacityProperty(), 1));
            KeyFrame key51r = new KeyFrame(Duration.millis(100), new KeyValue(c6.rotateProperty(), 0));

            timeline.getKeyFrames().addAll(key, key111, key111r);
            timeline.play();
            timeline1.getKeyFrames().addAll(key11, key12, key13, key11r);
            timeline.setOnFinished(a -> timeline1.play());
            timeline2.getKeyFrames().addAll(key21, key22, key23, key21r);
            timeline1.setOnFinished(a -> timeline2.play());
            timeline3.getKeyFrames().addAll(key3, key44, key31r);
            timeline2.setOnFinished(a -> timeline3.play());
            timeline4.getKeyFrames().addAll(key41, key42, key43, key41r);
            timeline3.setOnFinished(a -> timeline4.play());
            timeline5.getKeyFrames().addAll(key51, key52, key53, key51r);
            timeline4.setOnFinished(a -> timeline5.play());
            ok = true;
}
}

public void closeM(){


        Timeline timeline = new Timeline();
        Timeline timeline1 = new Timeline();
        Timeline timeline2 = new Timeline();
        Timeline timeline3 = new Timeline();
        Timeline timeline4 = new Timeline();
        Timeline timeline5 = new Timeline();
        Timeline timeline6 = new Timeline();
        KeyFrame key = new KeyFrame(Duration.millis(100), new KeyValue(c1.opacityProperty(), 0));
        KeyFrame key111 = new KeyFrame(Duration.millis(100), new KeyValue(c1.translateXProperty(), -100));

        KeyFrame key11 = new KeyFrame(Duration.millis(100), new KeyValue(c2.translateXProperty(), -2));
        KeyFrame key12 = new KeyFrame(Duration.millis(100), new KeyValue(c2.translateYProperty(), -2));
        KeyFrame key13 = new KeyFrame(Duration.millis(100), new KeyValue(c2.opacityProperty(), 0));

        KeyFrame key21 = new KeyFrame(Duration.millis(100), new KeyValue(c3.translateXProperty(), -2));
        KeyFrame key22 = new KeyFrame(Duration.millis(100), new KeyValue(c3.translateYProperty(), -2));
        KeyFrame key23 = new KeyFrame(Duration.millis(100), new KeyValue(c3.opacityProperty(), 0));

        KeyFrame key3 = new KeyFrame(Duration.millis(100),
                new KeyValue(c4.opacityProperty(), 0));

        KeyFrame key44 = new KeyFrame(Duration.millis(100), new KeyValue(c4.translateXProperty(), 120));

        KeyFrame key41 = new KeyFrame(Duration.millis(100), new KeyValue(c5.translateXProperty(), 2));
        KeyFrame key42 = new KeyFrame(Duration.millis(100), new KeyValue(c5.translateYProperty(), 2));
        KeyFrame key43 = new KeyFrame(Duration.millis(100), new KeyValue(c5.opacityProperty(), 0));

        KeyFrame key51 = new KeyFrame(Duration.millis(100), new KeyValue(c6.translateXProperty(), 2));
        KeyFrame key52 = new KeyFrame(Duration.millis(100), new KeyValue(c6.translateYProperty(), 2));
        KeyFrame key53 = new KeyFrame(Duration.millis(100), new KeyValue(c6.opacityProperty(), 0));

        KeyFrame key6 = new KeyFrame(Duration.millis(300),
                new KeyValue(b.opacityProperty(), 0));

        timeline.getKeyFrames().addAll(key, key111);
        timeline.play();
        timeline1.getKeyFrames().addAll(key11, key12, key13);
        timeline.setOnFinished(a -> timeline1.play());
        timeline2.getKeyFrames().addAll(key21, key22, key23);
        timeline1.setOnFinished(a -> timeline2.play());
        timeline3.getKeyFrames().addAll(key3, key44);
        timeline2.setOnFinished(a -> timeline3.play());
        timeline4.getKeyFrames().addAll(key41, key42, key43);
        timeline3.setOnFinished(a -> timeline4.play());
        timeline5.getKeyFrames().addAll(key51, key52, key53);
        timeline4.setOnFinished(a -> timeline5.play());
        timeline6.getKeyFrames().addAll(key6);
        timeline5.setOnFinished(x -> timeline6.play());
        timeline6.setOnFinished(a -> {
            JavaFXApplication3.stage.close();
            Accueil.a.getStage().getScene().getRoot().setEffect(null);
        });}


}
