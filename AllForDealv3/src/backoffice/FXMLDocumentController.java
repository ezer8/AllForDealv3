/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backoffice;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.layout.*;

/**
 *
 * @author Armin
 */
public class FXMLDocumentController implements Initializable {
    public static String center ;
    @FXML
    private BorderPane bp;
    private Parent ap;
    
    @FXML
    private void dashboardAction(ActionEvent e) throws IOException{
        ap = new FXMLLoader().load(getClass().getResource("FXMLDashboard.fxml"));
            bp.setCenter(ap);
    }
    @FXML
    private void adminAction(ActionEvent event) throws IOException {
        ap = new FXMLLoader().load(getClass().getResource("FXMLAdmins.fxml"));
        bp.setCenter(ap);
    }
    @FXML
    private void memberAction(ActionEvent e) throws IOException {
        ap = new FXMLLoader().load(getClass().getResource("FXMLMembers.fxml"));
        bp.setCenter(ap);
    }
    @FXML
    private void AProductsAction(ActionEvent e) throws IOException {
        ap = FXMLLoader.load(getClass().getResource("FXMLAllProducts.fxml"));
        center = "AllProducts";
        bp.setCenter(ap);
    }

    @FXML
    private void POAction(ActionEvent e) throws IOException {
        ap = FXMLLoader.load(getClass().getResource("FXMLProductsOffers.fxml"));
        center = "";
        bp.setCenter(ap);
    }

    @FXML
    private void reclamationAction(ActionEvent e) throws IOException {
        ap = FXMLLoader.load(getClass().getResource("FXMLReclamations.fxml"));
        bp.setCenter(ap);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }

}
