/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backoffice;

import edu.esprit.pidev.dao.classes.ReclamationDao;
import edu.esprit.pidev.entities.Reclamation;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Armin
 */
public class FXMLReclamationsController implements Initializable {
    public static FXMLReclamationsController c;
    public Stage secondaryStage;
    @FXML
    private TableView table;
    @FXML
    private TableColumn user,titre,date;
    @FXML
    private TextArea description;
    private ReclamationDao recDao;
    private ObservableList<Reclamation> data;
    
    
    @FXML
    private void resolveAction(ActionEvent e) throws IOException{
        if (table.getSelectionModel().getSelectedItem()!=null){
        Parent root = FXMLLoader.load(getClass().getResource("FXMLReclamationAffiche.fxml"));
        Scene scene = new Scene(root);
        secondaryStage.setScene(scene);
        secondaryStage.show();
        }else{
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("You have to select an item first !!");
            alert.showAndWait();
        }
        
    }
    @FXML
    private void removeAction(ActionEvent e){
        if (table.getSelectionModel().getSelectedItem()!=null){
            Reclamation rec = (Reclamation) table.getSelectionModel().getSelectedItem();
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setHeaderText("Are you sure ??");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                
                recDao.delete(rec);
                refresh();
            }
        }else{
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("You have to select an item first !!");
            alert.showAndWait();
        }
        
    }
    public void refresh(){
        data = FXCollections.observableArrayList(recDao.selectAll());
        table.setItems(data);
    }
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        c = this;
        secondaryStage = new Stage();
        recDao = new ReclamationDao();
        data = FXCollections.observableArrayList(recDao.selectAll());
        titre.setCellValueFactory(new PropertyValueFactory("titre"));
        user.setCellValueFactory(new PropertyValueFactory("user"));
        date.setCellValueFactory(new PropertyValueFactory("date"));
        table.setItems(data);
        table.getSelectionModel().selectedItemProperty().addListener((obs, oldS, newS) -> {
            if (obs.getValue()!=null){
            Reclamation rec = (Reclamation) obs.getValue();
            description.setText(rec.getDescription());
            }
        });
    }
    public Reclamation getRec(){
        return (Reclamation)table.getSelectionModel().getSelectedItem();
    }

    
    
}
