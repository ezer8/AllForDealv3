/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backoffice;

import edu.esprit.pidev.entities.Reclamation;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.text.*;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * FXML Controller class
 *
 * @author Seif
 */
public class FXMLReclamationAfficheController implements Initializable {
    @FXML
    private Label user,date,img,titre;
    @FXML
    private TextArea description,reply;
    @FXML
    private void sendAction(ActionEvent e) throws MessagingException{
        Properties props=new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        
        Session session=Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication(){
                        return new PasswordAuthentication("AllForDealPIDev@gmail.com", "allfordeal");
                    }
                    }
                );
        
       try {
            Message message=new MimeMessage(session);
            message.setFrom(new InternetAddress("AllForDealPIDev@gmail.com"));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("seifeddine.oueslati@esprit.tn"));
            message.setSubject("AllForDeal reply");
            message.setText(reply.getText());
            Transport.send(message);
            FXMLReclamationsController.c.secondaryStage.close();
       } catch(Exception ex){
           
       }
            
        
    }
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        Reclamation rec = FXMLReclamationsController.c.getRec();
        System.out.println("rec ="+rec.getMembres());
        user.setText(rec.getMembres().getNom()+" "+rec.getMembres().getPrenom()+" <"+rec.getMembres().getEmail()+">");
        titre.setText(rec.getTitre());
        date.setText(String.valueOf(rec.getDate()));
        description.setText(rec.getDescription());
    }    
    
}
