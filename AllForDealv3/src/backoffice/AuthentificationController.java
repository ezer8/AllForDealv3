/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backoffice;

import edu.esprit.pidev.dao.classes.MembreDao;
import edu.esprit.pidev.entities.Membre;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;

/**
 * FXML Controller class
 *
 * @author Blech tbarbich
 */
public class AuthentificationController implements Initializable {

    /**
     * Initializes the controller class.
     */
    public static AuthentificationController c ;
    public static String connection;
    Stage stage;
    public Membre m ;
    private Window primaryStage;
    @FXML 
    private TextField PseudoTextField;
    
    @FXML 
    private PasswordField MDPTextField;
    private MyBrowser myBrowser;
    private Scene scene;
    Stage browserStage;
    
    @FXML 
    private void HandleConnexion(){
        String pseudo = PseudoTextField.getText();
        String MDP=MDPTextField.getText();
       
        MembreDao membreDao =new MembreDao();
        m=membreDao.Authentification(pseudo, MDP);
        if(m != null){
            showInterface();
    }
        else{
            Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(primaryStage);
            alert.setTitle("Not found");
            alert.setHeaderText("Not found");
            alert.setContentText("Pseudo ou mot de passe incorrect.");
            alert.showAndWait();
        }
        
    }
    public Stage getStage(){
        return browserStage;
    }
    @FXML
    private void FBConnexion(ActionEvent e){
        myBrowser = new MyBrowser();
        scene = new Scene(myBrowser, 640, 480);
        browserStage = new Stage();
        browserStage.setScene(scene);
        browserStage.show();
        connection = "FB";
    }
    @FXML 
    private void HandleInscription() throws IOException{
        connection = "signup";
        Parent p = FXMLLoader.load(getClass().getResource("FXMLInscription.fxml"));
        Scene scene = new Scene(p);
        stage = new Stage();
        stage.setScene(scene);
        stage.show();
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        c = this;
        // TODO
    }    

    private void showInterface() {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
            // Create the dialog Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Admin");
            Scene scene = new Scene(root);
            dialogStage.setScene(scene);
            dialogStage.show();
        } catch (IOException e) {
        }
    }
    
    
}
