/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backoffice;

import edu.esprit.pidev.dao.classes.AdministrateurDao;
import edu.esprit.pidev.entities.Administrateur;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Armin
 */
public class FXMLAdminChangeAfficheController implements Initializable {
    Administrateur admin;
    @FXML
    private TextField login,nom,prenom;
    @FXML
    private PasswordField password;
    @FXML
    private void submitAction(ActionEvent e){
        AdministrateurDao aDao = new AdministrateurDao();  
        admin.setLogin(login.getText());
        admin.setPassword(password.getText());
        admin.setNom(nom.getText());
        admin.setPrenom(prenom.getText());
        aDao.update(admin);
        FXMLAdminsController.secondaryStage.close();
        FXMLAdminsController.c.refresh();
    }
    @FXML
    private void resetAction(ActionEvent e){
        login.setText("");
        password.setText("");
        nom.setText("");
        prenom.setText("");
    }
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        admin = FXMLAdminsController.c.getSelectedAdmin();
        login.setText(admin.getLogin());
        password.setText(admin.getPassword());
        nom.setText(admin.getNom());
        prenom.setText(admin.getPrenom());
    }    
    
}
