/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backoffice;

import edu.esprit.pidev.dao.classes.AdministrateurDao;
import edu.esprit.pidev.entities.Administrateur;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Armin
 */
public class FXMLAdminsController implements Initializable {
    ObservableList<Administrateur> data;
    AdministrateurDao aDao;
    public static Stage secondaryStage;
    @FXML
    private TableView adminTable;
    @FXML
    private TableColumn loginC, passwordC, nomC, prenomC;
    @FXML
    private AnchorPane ap1;
    static FXMLAdminsController c;
    @FXML
    private void AddAction(ActionEvent e) throws IOException {
        createStage(secondaryStage, "FXMLAdminAffiche.fxml");
    }
    
    @FXML
    private void ChangeAction(ActionEvent e) throws IOException {
        if (getSelectedAdmin()==null){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setContentText("You have to select Item first !!");
        alert.showAndWait();
        }else 
            createStage(secondaryStage, "FXMLAdminChangeAffiche.fxml");
    }

    @FXML
    private void RemoveAction(ActionEvent e) {
        if (getSelectedAdmin()==null){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setContentText("You have to Item someone first !!");
        alert.showAndWait();
        }else {
        Administrateur admin = (Administrateur) adminTable.getSelectionModel().getSelectedItem();
        aDao.delete(admin);
        refresh();
        }
    }

    public void createStage(Stage stage, String file) throws IOException {

        Parent root = FXMLLoader.load(getClass().getResource(file));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Initializes the controller class.
     */

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        secondaryStage = new Stage();
        c = this;
        aDao = new AdministrateurDao();
        data = FXCollections.observableArrayList(aDao.selectAll());
        loginC.setCellValueFactory(new PropertyValueFactory<>("login"));
        passwordC.setCellValueFactory(new PropertyValueFactory<>("password"));
        nomC.setCellValueFactory(new PropertyValueFactory<>("nom"));
        prenomC.setCellValueFactory(new PropertyValueFactory<>("prenom"));
        adminTable.setItems(data);
        

    }

    public void refresh(){
        data = FXCollections.observableArrayList(aDao.selectAll());
        adminTable.setItems(data);
    }
    public Administrateur getSelectedAdmin(){
        return (Administrateur) adminTable.getSelectionModel().getSelectedItem();
    }
   

}
