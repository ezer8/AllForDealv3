/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backoffice;

import edu.esprit.pidev.dao.classes.ProduitDao;
import edu.esprit.pidev.entities.Produit;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Armin
 */
public class FXMLProductsOffersController implements Initializable {

    public static FXMLProductsOffersController c;
    private Stage secondaryStage;
    private ProduitDao pDao;
    private ObservableList<Produit> data;
    @FXML
    private TableView table;
    @FXML
    private TableColumn libelle, user, categorie, prix, date;
    @FXML
    private TextArea description;

    /**
     * Initializes the controller class.
     */
    @FXML
    private void changeAction(ActionEvent e) throws IOException {
        if (table.getSelectionModel().getSelectedItem()!=null){
        Parent root = FXMLLoader.load(getClass().getResource("FXMLProductAffiche.fxml"));
        Scene scene = new Scene(root);
        secondaryStage.setScene(scene);
        secondaryStage.show();
        }else{
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("You have to select an item first !!");
            alert.showAndWait();
        }
        
    }
    private void refresh(){
        data = FXCollections.observableArrayList(pDao.selectByEtat("enAttente"));
        table.setItems(data);
    }
    @FXML
    private void accepteAction(ActionEvent e) {
        if (table.getSelectionModel().getSelectedItem() != null) {
            Produit p = (Produit) table.getSelectionModel().getSelectedItem();
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setHeaderText("Are you sure ??");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                p.setEtat("Accepte");
                pDao.update(p);
                refresh();
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("You have to select an item first !!");
            alert.showAndWait();
        }
    }

    @FXML
    private void refuseAction(ActionEvent e) {
        if (table.getSelectionModel().getSelectedItem()!=null){
            Produit p = (Produit) table.getSelectionModel().getSelectedItem();
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setHeaderText("Are you sure ??");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                p.setEtat("Refuse");
                pDao.update(p);
                refresh();
            }
        }else{
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("You have to select an item first !!");
            alert.showAndWait();
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        c = this;
        secondaryStage = new Stage();
        pDao = new ProduitDao();
        data = FXCollections.observableArrayList(pDao.selectByEtat("enAttente"));
        libelle.setCellValueFactory(new PropertyValueFactory("libelle"));
        user.setCellValueFactory(new PropertyValueFactory("user"));
        categorie.setCellValueFactory(new PropertyValueFactory("categorieCol"));
        prix.setCellValueFactory(new PropertyValueFactory("prix"));
        date.setCellValueFactory(new PropertyValueFactory("date"));
        table.setItems(data);
        table.getSelectionModel().selectedItemProperty().addListener((obs, oldS, newS) -> {
            if (obs.getValue()!=null){
            Produit p = (Produit) obs.getValue();
            description.setText(p.getDescription());
            }
        });
    }

    public Produit getProduit() {
        return (Produit) table.getSelectionModel().getSelectedItem();
    }
}
