/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backoffice;

import edu.esprit.pidev.entities.Produit;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Armin
 */
public class FXMLProductAfficheController implements Initializable {
    @FXML
    private Label date,user;
    @FXML
    private TextField libelle,prix,categorie;
    @FXML
    private TextArea description;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        Produit p;
        if (FXMLDocumentController.center.equals("AllProducts"))
            p = FXMLAllProductsController.c.getProduit();
        else
            p = FXMLProductsOffersController.c.getProduit();
        String d = String.valueOf(p.getDate());
        date.setText(d);
        user.setText(p.getUser());
        libelle.setText(p.getLibelle());
        prix.setText(String.valueOf(p.getPrix()));
        categorie.setText(p.getCategorieCol());
        description.setText(p.getDescription());
    }
    
    
}
