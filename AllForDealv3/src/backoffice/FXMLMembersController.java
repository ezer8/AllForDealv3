/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backoffice;

import edu.esprit.pidev.dao.classes.MembreDao;
import edu.esprit.pidev.entities.Membre;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author Armin
 */
public class FXMLMembersController implements Initializable {
    @FXML
    private TableView table;
    @FXML
    private TableColumn nom,prenom,login,password,date;
    private MembreDao mdao;
    private ObservableList<Membre> data;
    
    
    
    
    @FXML
    private void deleteAction(ActionEvent e){
        Membre m =(Membre)table.getSelectionModel().getSelectedItem();
        mdao.delete(m);
        refresh();
    }
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        mdao = new MembreDao();
        data = FXCollections.observableArrayList(mdao.selectAll());
        nom.setCellValueFactory(new PropertyValueFactory<>("nom"));
        prenom.setCellValueFactory(new PropertyValueFactory<>("prenom"));
        login.setCellValueFactory(new PropertyValueFactory<>("login"));
        password.setCellValueFactory(new PropertyValueFactory<>("password"));
        date.setCellValueFactory(new PropertyValueFactory<>("dateVisite"));
        table.setItems(data);
    }    

    private void refresh() {
        data = FXCollections.observableArrayList(mdao.selectAll());
        table.setItems(data);
    }
    
}
