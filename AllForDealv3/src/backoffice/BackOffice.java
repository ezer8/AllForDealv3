/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backoffice;

import edu.esprit.pidev.dao.classes.AdministrateurDao;
import edu.esprit.pidev.entities.Administrateur;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.stage.Stage;

/**
 *
 * @author Armin
 */
public class BackOffice extends Application {
    public static BackOffice app;
    private Parent bp;
    Stage primaryStage;
    
    @Override
    public void start(Stage stage) throws Exception {
        app = this;
        bp = FXMLLoader.load(getClass().getResource("Authentification.fxml"));
        Scene scene = new Scene(bp);
        primaryStage = new Stage();
        primaryStage.setScene(scene);
        primaryStage.show();
        System.out.println("stage1="+primaryStage);
        //System.out.println(ap1.getWidth());
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
       
    }
    
}
