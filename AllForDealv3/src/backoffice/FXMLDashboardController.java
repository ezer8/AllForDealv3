/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backoffice;

import edu.esprit.pidev.dao.classes.MembreDao;
import edu.esprit.pidev.dao.classes.ProduitDao;
import edu.esprit.pidev.entities.Membre;
import edu.esprit.pidev.entities.Produit;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.*;

/**
 * FXML Controller class
 *
 * @author Yahyaoui Ghassen
 */
public class FXMLDashboardController implements Initializable {
    @FXML
    private PieChart pie;
    @FXML
    private BarChart<String,Integer> bar;
    @FXML
    private LineChart<String,Integer> line;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();
        ObservableList<XYChart.Data> data = FXCollections.observableArrayList();
        ProduitDao pdao = new ProduitDao();
        MembreDao mdao = new MembreDao();
        for(Produit p : pdao.selectTop(4)){
            pieChartData.add(new PieChart.Data(p.getLibelle(),p.getNbVente()));
            
        }
        for(Membre m : mdao.selectTop(3))
            data.add(new XYChart.Data(m.getLogin(), m.getNbProdVendu()));
         pie.setData(pieChartData);
         XYChart.Series serie1 = new XYChart.Series(data);
         bar.getData().setAll(serie1);
         bar.setLegendVisible(false);
         ObservableList<XYChart.Data> data2 = FXCollections.observableArrayList(
                 new XYChart.Data("jan",10),
                 new XYChart.Data("fev",20),
                 new XYChart.Data("mars",30),
                 new XYChart.Data("avril",40),
                 new XYChart.Data("mai",50),
                 new XYChart.Data("juin",60),
                 new XYChart.Data("juillet",70),
                 new XYChart.Data("aout",80),
                 new XYChart.Data("sept",90),
                 new XYChart.Data("oct",100),
                 new XYChart.Data("nov",110),
                 new XYChart.Data("dec",120)
         );
         ObservableList<XYChart.Data> data3 = FXCollections.observableArrayList(
                 new XYChart.Data("jan",80),
                 new XYChart.Data("fev",30),
                 new XYChart.Data("mars",300),
                 new XYChart.Data("avril",400),
                 new XYChart.Data("mai",550),
                 new XYChart.Data("juin",100),
                 new XYChart.Data("juillet",77),
                 new XYChart.Data("aout",80),
                 new XYChart.Data("sept",999),
                 new XYChart.Data("oct",15),
                 new XYChart.Data("nov",50),
                 new XYChart.Data("dec",12)
         );
         XYChart.Series serie2 = new XYChart.Series(data2);
         XYChart.Series serie3 = new XYChart.Series(data3);
         serie2.setName("sales");
         serie3.setName("nb de visiteurs");
         line.getData().setAll(serie2,serie3);
         
    }    
    
}
