/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package backoffice;

import edu.esprit.pidev.dao.classes.AdministrateurDao;
import edu.esprit.pidev.entities.Administrateur;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Armin
 */
public class FXMLAdminAfficheController implements Initializable {
    @FXML
    private TextField login,nom,prenom;
    @FXML
    private PasswordField password;
    @FXML
    private void submitAction(ActionEvent e){
        AdministrateurDao aDao = new AdministrateurDao();
        Administrateur admin = new Administrateur(login.getText(),password.getText(),nom.getText(),prenom.getText());
        aDao.add(admin);
        FXMLAdminsController.secondaryStage.close();
        FXMLAdminsController.c.refresh();
    }
    @FXML
    private void resetAction(ActionEvent e){
        
    }
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
    }    
    
}
