/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.esprit.pidev.dao.classes;

import edu.esprit.pidev.dao.interfaces.IAchatDao;
import edu.esprit.pidev.entities.Achat;
import edu.esprit.pidev.entities.Membre;
import edu.esprit.pidev.technique.DataSource;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author GhassenNasri
 */
public class AchatDao implements IAchatDao<Achat, Membre> {

    private Connection connection;

    public AchatDao() {
        connection = DataSource.getInstance().getConnection();

    }

    @Override
    public int add(Achat achat) {
        try {
            String req = "insert into achat (date,idmembre,produits,total) values (?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setDate(1, achat.getDate());
            ps.setInt(2, achat.getMembre().getId());
            ps.setString(3, achat.getProduits());
            ps.setFloat(4, achat.getTotal());
            return ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ServiceDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    @Override
    public int update(Achat achat) {
        try {
            String req = "update achat set date = ? ,idmembre = ?,produits = ?,total = ? where id = ?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setDate(1, achat.getDate());
            ps.setInt(2, achat.getMembre().getId());
            ps.setString(3, achat.getProduits());
            ps.setFloat(4, achat.getTotal());
            return ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ServiceDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    @Override
    public int delete(Achat achat) {
        try {
            String req = "delete from achat where id=?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, achat.getId());
            return ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ServiceDao.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    @Override
    public List<Achat> selectAll() {
        String req = "select * from achat ";
        MembreDao membreDao = new MembreDao();
        List<Achat> achats = new ArrayList();

        try {
            PreparedStatement ps = connection.prepareStatement(req);
            ResultSet resultat = ps.executeQuery();

            while (resultat.next()) {
                Achat achat = new Achat();
                achat.setId(resultat.getInt(1));
                achat.setDate(resultat.getDate(2));
                achat.setMembre(membreDao.selectById(resultat.getInt(3)));
                achat.setProduits(resultat.getString(4));
                achat.setTotal(resultat.getFloat(5));
                achats.add(achat);
            }
            return achats;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement" + ex.getMessage());
            return null;
        }
    }

    @Override
    public Achat selectByID(int id) {
        String req = "select * from achat where id=?";
        MembreDao membreDao = new MembreDao();
        Achat achat = new Achat();

        try {
            PreparedStatement ps = connection.prepareStatement(req);
            ResultSet resultat = ps.executeQuery();

            while (resultat.next()) {
                achat.setId(resultat.getInt(1));
                achat.setDate(resultat.getDate(2));
                achat.setMembre(membreDao.selectById(resultat.getInt(3)));
                achat.setProduits(resultat.getString(4));
                achat.setTotal(resultat.getFloat(5));
            }
            return achat;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement" + ex.getMessage());
            return null;
        }
    }

    @Override
    public List<Achat> selectByDate(Date date) {
        String req = "select * from achat where date=?";
        MembreDao membreDao = new MembreDao();
        List<Achat> achats = new ArrayList();
        Achat achat = new Achat();

        try {
            PreparedStatement ps = connection.prepareStatement(req);
            ResultSet resultat = ps.executeQuery();

            while (resultat.next()) {
                achat.setId(resultat.getInt(1));
                achat.setDate(resultat.getDate(2));
                achat.setMembre(membreDao.selectById(resultat.getInt(3)));
                achat.setProduits(resultat.getString(4));
                achat.setTotal(resultat.getFloat(5));
                achats.add(achat);
            }
            return achats;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement" + ex.getMessage());
            return null;
        }
    }

    @Override
    public List<Achat> selectByUser(Membre user) {
        String req = "select * from achat where idmembre=?";
        MembreDao membreDao = new MembreDao();
        List<Achat> achats = new ArrayList();
        Achat achat = new Achat();

        try {
            PreparedStatement ps = connection.prepareStatement(req);
            ResultSet resultat = ps.executeQuery();

            while (resultat.next()) {
                achat.setId(resultat.getInt(1));
                achat.setDate(resultat.getDate(2));
                achat.setMembre(membreDao.selectById(resultat.getInt(3)));
                achat.setProduits(resultat.getString(4));
                achat.setTotal(resultat.getFloat(5));
                achats.add(achat);
            }
            return achats;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement" + ex.getMessage());
            return null;
        }    }

}