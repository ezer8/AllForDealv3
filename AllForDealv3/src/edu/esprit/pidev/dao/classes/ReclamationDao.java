/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.esprit.pidev.dao.classes;

import edu.esprit.pidev.dao.interfaces.IReclamationDao;
import edu.esprit.pidev.entities.Membre;
import edu.esprit.pidev.entities.Produit;
import edu.esprit.pidev.entities.Reclamation;
import edu.esprit.pidev.technique.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Asus
 */
public class ReclamationDao implements IReclamationDao<Reclamation> {

    private Connection conn;

    public ReclamationDao() {
        conn = DataSource.getInstance().getConnection();
    }
    
    @Override
    public int add(Reclamation reclamation) {
        try {
            String req = "insert into reclamation values(?,?,?,?)";
            PreparedStatement ps = conn.prepareStatement(req);
            ps.setString(1, reclamation.getTitre());
            ps.setString(2, reclamation.getDescription());
            ps.setDate(3, reclamation.getDate());
            ps.setInt(4, reclamation.getMembres().getId());
            return ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ReclamationDao.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
        
    }

    @Override
    public int update(Reclamation reclamation) {
        try {
            String req = "update reclamation set titre=?,description=?,date=?,idmembre=? where id=?";
            PreparedStatement ps = conn.prepareStatement(req);
            ps.setString(1, reclamation.getTitre());
            ps.setString(2, reclamation.getDescription());
            ps.setDate(3, reclamation.getDate());
            ps.setInt(4, reclamation.getMembres().getId());
            return ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ReclamationDao.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    @Override
    public int delete(Reclamation reclamation) {
        try {
            String req = "delete from reclamation where id=?";
            PreparedStatement ps = conn.prepareStatement(req);
            ps.setInt(1, reclamation.getId());
            return ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ReclamationDao.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
        
    }

    @Override
    public Reclamation selectById(int id) {
        Reclamation rec = null;
        try {
            String req = "select from reclamation where id =?";
            PreparedStatement ps = conn.prepareStatement(req);
            ps.setInt(1,id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()){
                MembreDao mdao = new MembreDao();
                Membre membre = mdao.selectById(rs.getInt("idmembre"));
                rec = new Reclamation(rs.getInt("id"), rs.getString("titre"),rs.getString("description"), rs.getDate("date"), membre);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ReclamationDao.class.getName()).log(Level.SEVERE, null, ex);
            
        }
        return rec;
         
    }

    @Override
    public List<Reclamation> selectAll() {
        List<Reclamation> list = new ArrayList<>();
        try {
            String req = "select * from reclamation";
            PreparedStatement ps = conn.prepareStatement(req);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                MembreDao mdao = new MembreDao();
                Membre membre = mdao.selectById(rs.getInt("idmembre"));
                Reclamation rec = new Reclamation(rs.getInt("id"), rs.getString("titre"),rs.getString("description"), rs.getDate("date"), membre);
                list.add(rec);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ReclamationDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
}
