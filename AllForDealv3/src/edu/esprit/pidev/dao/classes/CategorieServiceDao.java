/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.esprit.pidev.dao.classes;

import edu.esprit.pidev.dao.interfaces.ICategorieDao;
import edu.esprit.pidev.entities.CategorieService;
import edu.esprit.pidev.entities.Service;
import edu.esprit.pidev.technique.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Asus
 */
public class CategorieServiceDao implements ICategorieDao<CategorieService>{
    
    private final Connection connection;

    public CategorieServiceDao() {
        connection = DataSource.getInstance().getConnection();
    }

    @Override
    public int add(CategorieService categorie) {
        try {
            String req = "insert into categorie_service (libelle) values (?)";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setString(1, categorie.getLibelle());
            
            
           return  ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProduitDao.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    @Override
    public int update(CategorieService categorie) {
        try {
            String req = "update categorie_service set libelle=? where id=?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setString(1, categorie.getLibelle());
            ps.setInt(2, categorie.getId());
            
            
           return  ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProduitDao.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    @Override
    public int delete(CategorieService categorie) {
        try {
            String req = "delete from categorie_service where id=?";
            PreparedStatement ps = connection.prepareStatement(req);
            
            ps.setInt(1, categorie.getId());
            
           return  ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProduitDao.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    @Override
    public List<CategorieService> selectAll() {
        List<CategorieService> categorieServices=new ArrayList<>();
     
        String requete = "select * from categorie_service";
        try {
            PreparedStatement ps = connection.prepareStatement(requete);
            ResultSet resultat = ps.executeQuery();
            while (resultat.next()) {
                CategorieService categorieService=new CategorieService();
                categorieService.setId(resultat.getInt(1));
                categorieService.setLibelle(resultat.getString(2));
                
                categorieServices.add(categorieService);
            }
            return categorieServices;

        } catch (SQLException ex) {
            System.out.println("erreur lors de la recherche du depot " + ex.getMessage());
            return null;
        }
    }

    @Override
    public CategorieService selectById(int id) {
        CategorieService categorieService = new CategorieService();
     
        String requete = "select * from categorie_service where id=?";
        try {
            PreparedStatement ps = connection.prepareStatement(requete);
            ps.setInt(1, id);
            ResultSet resultat = ps.executeQuery();
            while (resultat.next()) {
                categorieService.setId(resultat.getInt(1));
                categorieService.setLibelle(resultat.getString(2));
                
                
            }
            return categorieService;

        } catch (SQLException ex) {
            System.out.println("erreur lors de la recherche du depot " + ex.getMessage());
            return null;
        }
    }

    @Override
    public List<CategorieService> selectByLibelle(String libelle) {
           List<CategorieService> categorieServices=new ArrayList<>();
     
        String requete = "select * from categorie_service where libelle=?";
        try {
            PreparedStatement ps = connection.prepareStatement(requete);
            ps.setString(1,libelle);
            ResultSet resultat = ps.executeQuery();
            while (resultat.next()) {
                CategorieService categorieService=new CategorieService();
                categorieService.setId(resultat.getInt(1));
                categorieService.setLibelle(resultat.getString(2));
                
               categorieServices.add(categorieService);
            }
            return categorieServices;

        } catch (SQLException ex) {
            System.out.println("erreur lors de la recherche du depot " + ex.getMessage());
            return null;
        }
    }

    
    
}
