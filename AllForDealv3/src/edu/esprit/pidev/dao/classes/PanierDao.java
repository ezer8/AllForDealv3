/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.esprit.pidev.dao.classes;

import edu.esprit.pidev.dao.interfaces.IPanierDao;
import edu.esprit.pidev.entities.Panier;
import edu.esprit.pidev.entities.Produit;
import edu.esprit.pidev.entities.Service;
import edu.esprit.pidev.technique.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author GhassenNasri
 */
public class PanierDao implements IPanierDao<Panier> {

    private Connection connection;

    public PanierDao() {
        connection = DataSource.getInstance().getConnection();

    }

    @Override
    public int add(Panier panier) {
        String req;
        try {
            for (Produit p : panier.getProduitsPanier().keySet()) {
                req = "insert into panier (idmembre,idproduit,quantite) values (?,?,?)";
                PreparedStatement ps = connection.prepareStatement(req);
                ps.setInt(1, panier.getMembre().getId());
                ps.setInt(2, p.getId());
                ps.setInt(3, panier.getProduitsPanier().get(p));
                ps.executeUpdate();
                
                
            }
            return 1;
        } catch (SQLException ex) {
            Logger.getLogger(ServiceDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    @Override
    public int update(Panier panier) {
        String req;
        try {
            if(panier.getProduitsPanier().isEmpty())
            delete(panier);
            for (Produit p : panier.getProduitsPanier().keySet()) {
                req = "update panier set quantite = ? where idmembre = ? AND idproduit = ? ";
                PreparedStatement ps = connection.prepareStatement(req);
                ps.setInt(3, p.getId());
                ps.setInt(1, panier.getProduitsPanier().get(p));
                ps.setInt(2, panier.getMembre().getId());
                ps.executeUpdate();
            }
            return 1;
        } catch (SQLException ex) {
            Logger.getLogger(ServiceDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    @Override
    public int delete(Panier panier) {
        String req;
        try {

            req = "delete from panier where idmembre=?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, panier.getMembre().getId());
            return ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ServiceDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    @Override
    public Panier selectById(int idMembre) {
        String req = "select * from panier where idmembre=?";
        MembreDao membreDao = new MembreDao();
        ProduitDao produitDao = new ProduitDao();
        Panier panier = new Panier();
        try {
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, idMembre);
            ResultSet resultat = ps.executeQuery();

            while (resultat.next()) {
                panier.setMembre(membreDao.selectById(resultat.getInt(1)));
                panier.ajouterProduitP(produitDao.selectById(resultat.getInt(2)), resultat.getInt(3));

            }
            return panier;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement" + ex.getMessage());
            return null;
        }
    }

    @Override
    public List<Panier> selectAll() {
        String req = "select * from panier";
        MembreDao membreDao = new MembreDao();
        ProduitDao produitDao = new ProduitDao();
        List<Panier> listPanier = new ArrayList<>();
        Panier panier = null;
        int id = 0;
        try {
            PreparedStatement ps = connection.prepareStatement(req);
            ResultSet resultat = ps.executeQuery();

            while (resultat.next()) {
                if (id != resultat.getInt(1)) {
                    if (panier != null) {
                        listPanier.add(panier);
                    }
                    panier = new Panier();
                    id = resultat.getInt(1);
                }

                panier.setMembre(membreDao.selectById(resultat.getInt(1)));
                panier.ajouterProduitP(produitDao.selectById(resultat.getInt(2)), 0);
            }
            return listPanier;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement" + ex.getMessage());
            return null;
        }
    }

}
