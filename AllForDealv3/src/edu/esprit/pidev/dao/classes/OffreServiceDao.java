/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.esprit.pidev.dao.classes;

import edu.esprit.pidev.dao.interfaces.IOffreServiceDao;
import edu.esprit.pidev.entities.CategorieService;
import edu.esprit.pidev.entities.Membre;
import edu.esprit.pidev.entities.OffreService;
import edu.esprit.pidev.entities.Service;
import edu.esprit.pidev.technique.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Asus
 */
public class OffreServiceDao implements IOffreServiceDao<OffreService,Membre, CategorieService, Service> {
    private Connection connection;

    public OffreServiceDao() {
        connection = DataSource.getInstance().getConnection();
    }
    @Override
    public int add(OffreService offre) {
          try {
            String req = "insert into offre_service (libelle,date,description,idmembre,idcategorie,idservice,etat) values (?,?,?,?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setString(1, offre.getLibelle());
            ps.setDate(2, offre.getDate());
            ps.setString(3, offre.getDescription());
           //ps.setInt(4, offre.getMembre().getId());
            ps.setInt(5, offre.getCategorieService().getId());
            ps.setInt(6, offre.getService().getId());
            ps.setString(7, offre.getEtat());
            return ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ServiceDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    

    @Override
    public int update(OffreService offre) {
         try {
            String req = "update offre_service set libelle = ?,date = ? ,description = ?,idmembre = ?,idcategorie = ? idservice = ? , etat = ? where id = ?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setString(1, offre.getLibelle());
            ps.setDate(2, offre.getDate());
            ps.setString(3, offre.getDescription());
           //ps.setInt(4, offre.getMembre().getId());
            ps.setInt(5, offre.getCategorieService().getId());
            ps.setInt(6, offre.getService().getId());
            ps.setString(7, offre.getEtat());
            ps.setInt(8, offre.getId());
            return ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ServiceDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    
    }

    @Override
    public int delete(OffreService offre) {
           try {
            String req = "delete from offre_service where id=?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, offre.getId());
            return ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ServiceDao.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    @Override
    public OffreService selectById(int id) {
           String req = "select * from offre_service where id=?";
            MembreDao membreDao = new MembreDao();
            CategorieServiceDao categorieServiceDao = new CategorieServiceDao();
            ServiceDao serviceDao = new ServiceDao();
            OffreService offreService = new OffreService();
        try {
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ResultSet resultat = ps.executeQuery();
           

            while (resultat.next()) {

                offreService.setId(resultat.getInt(1));
                offreService.setLibelle(resultat.getString(2));
                offreService.setDate(resultat.getDate(3));
                offreService.setDescription(resultat.getString(4));
                offreService.setMembre(membreDao.selectById(resultat.getInt(5)));
                offreService.setCategorieService(categorieServiceDao.selectById(resultat.getInt(6)));
                offreService.setService(serviceDao.selectById(resultat.getInt(7)));
                offreService.setEtat(resultat.getString(8));
            }
            return offreService;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement" + ex.getMessage());
            return null;
        }
    }

    @Override
    public OffreService selectByCode(int code) {
      String req = "select * from offre_service where code=?";
            MembreDao membreDao = new MembreDao();
            CategorieServiceDao categorieServiceDao = new CategorieServiceDao();
            ServiceDao serviceDao = new ServiceDao();
            OffreService offreService = new OffreService();
        try {
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, code);
            ResultSet resultat = ps.executeQuery();
           

            while (resultat.next()) {

                offreService.setId(resultat.getInt(1));
                offreService.setLibelle(resultat.getString(2));
                offreService.setDate(resultat.getDate(3));
                offreService.setDescription(resultat.getString(4));
                offreService.setMembre(membreDao.selectById(resultat.getInt(5)));
                offreService.setCategorieService(categorieServiceDao.selectById(resultat.getInt(6)));
                offreService.setService(serviceDao.selectById(resultat.getInt(7)));
                offreService.setEtat(resultat.getString(8));
            }
            return offreService;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement" + ex.getMessage());
            return null;
        }
    }

    @Override
    public OffreService selectByUser(Membre user) {
      String req = "select * from offre_service where idmembre=?";
            MembreDao membreDao = new MembreDao();
            CategorieServiceDao categorieServiceDao = new CategorieServiceDao();
            ServiceDao serviceDao = new ServiceDao();
            OffreService offreService = new OffreService();
        try {
            PreparedStatement ps = connection.prepareStatement(req);
            //ps.setInt(1, user.getId());
            ResultSet resultat = ps.executeQuery();
           

            while (resultat.next()) {

                offreService.setId(resultat.getInt(1));
                offreService.setLibelle(resultat.getString(2));
                offreService.setDate(resultat.getDate(3));
                offreService.setDescription(resultat.getString(4));
                offreService.setMembre(membreDao.selectById(resultat.getInt(5)));
                offreService.setCategorieService(categorieServiceDao.selectById(resultat.getInt(6)));
                offreService.setService(serviceDao.selectById(resultat.getInt(7)));
                offreService.setEtat(resultat.getString(8));
            }
            return offreService;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement" + ex.getMessage());
            return null;
        }
    }

    @Override
    public List<OffreService> selectByCategorie(CategorieService categorie) {
         String req = "select * from offre_service where idcategorie=?";
            MembreDao membreDao = new MembreDao();
            CategorieServiceDao categorieServiceDao = new CategorieServiceDao();
            ServiceDao serviceDao = new ServiceDao();
            List<OffreService> offreServices = new ArrayList<>();
        try {
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, categorie.getId());
            ResultSet resultat = ps.executeQuery();
           

            while (resultat.next()) {
                OffreService offreService = new OffreService();
                offreService.setId(resultat.getInt(1));
                offreService.setLibelle(resultat.getString(2));
                offreService.setDate(resultat.getDate(3));
                offreService.setDescription(resultat.getString(4));
                offreService.setMembre(membreDao.selectById(resultat.getInt(5)));
                offreService.setCategorieService(categorieServiceDao.selectById(resultat.getInt(6)));
                offreService.setService(serviceDao.selectById(resultat.getInt(7)));
                offreService.setEtat(resultat.getString(8));
                offreServices.add(offreService);
            }
            return offreServices;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement" + ex.getMessage());
            return null;
        }
    }

    @Override
    public List<OffreService> selectAll() {
          String req = "select * from offre_service ";
            MembreDao membreDao = new MembreDao();
            CategorieServiceDao categorieServiceDao = new CategorieServiceDao();
            ServiceDao serviceDao = new ServiceDao();
            List<OffreService> offreServices = new ArrayList<>();
        try {
            PreparedStatement ps = connection.prepareStatement(req);
            ResultSet resultat = ps.executeQuery();
           

            while (resultat.next()) {
                OffreService offreService = new OffreService();
                offreService.setId(resultat.getInt(1));
                offreService.setLibelle(resultat.getString(2));
                offreService.setDate(resultat.getDate(3));
                offreService.setDescription(resultat.getString(4));
                offreService.setMembre(membreDao.selectById(resultat.getInt(5)));
                offreService.setCategorieService(categorieServiceDao.selectById(resultat.getInt(6)));
                offreService.setService(serviceDao.selectById(resultat.getInt(7)));
                offreService.setEtat(resultat.getString(8));
                offreServices.add(offreService);
            }
            return offreServices;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement" + ex.getMessage());
            return null;
        }
    }

    @Override
    public List<OffreService> selectByLibelle(String libelle) {
        String req = "select * from offre_service where libelle=?";
            MembreDao membreDao = new MembreDao();
            CategorieServiceDao categorieServiceDao = new CategorieServiceDao();
            ServiceDao serviceDao = new ServiceDao();
            List<OffreService> offreServices = new ArrayList<>();
        try {
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setString(1, libelle);
            ResultSet resultat = ps.executeQuery();
           

            while (resultat.next()) {
                OffreService offreService = new OffreService();
                offreService.setId(resultat.getInt(1));
                offreService.setLibelle(resultat.getString(2));
                offreService.setDate(resultat.getDate(3));
                offreService.setDescription(resultat.getString(4));
                offreService.setMembre(membreDao.selectById(resultat.getInt(5)));
                offreService.setCategorieService(categorieServiceDao.selectById(resultat.getInt(6)));
                offreService.setService(serviceDao.selectById(resultat.getInt(7)));
                offreService.setEtat(resultat.getString(8));
                offreServices.add(offreService);
            }
            return offreServices;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement" + ex.getMessage());
            return null;
        }
    }

    @Override
    public List<OffreService> selectByDate(Date date) {
         String req = "select * from offre_service where date=?";
            MembreDao membreDao = new MembreDao();
            CategorieServiceDao categorieServiceDao = new CategorieServiceDao();
            ServiceDao serviceDao = new ServiceDao();
            List<OffreService> offreServices = new ArrayList<>();
        try {
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setDate(1, date);
            ResultSet resultat = ps.executeQuery();
           

            while (resultat.next()) {
                OffreService offreService = new OffreService();
                offreService.setId(resultat.getInt(1));
                offreService.setLibelle(resultat.getString(2));
                offreService.setDate(resultat.getDate(3));
                offreService.setDescription(resultat.getString(4));
                offreService.setMembre(membreDao.selectById(resultat.getInt(5)));
                offreService.setCategorieService(categorieServiceDao.selectById(resultat.getInt(6)));
                offreService.setService(serviceDao.selectById(resultat.getInt(7)));
                offreService.setEtat(resultat.getString(8));
                offreServices.add(offreService);
            }
            return offreServices;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement" + ex.getMessage());
            return null;
        }
    }

    @Override
    public List<OffreService> selectByEtat(String etat) {
       String req = "select * from offre_service where etat=?";
            MembreDao membreDao = new MembreDao();
            CategorieServiceDao categorieServiceDao = new CategorieServiceDao();
            ServiceDao serviceDao = new ServiceDao();
            List<OffreService> offreServices = new ArrayList<>();
        try {
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setString(1, etat);
            ResultSet resultat = ps.executeQuery();
           

            while (resultat.next()) {
                OffreService offreService = new OffreService();
                offreService.setId(resultat.getInt(1));
                offreService.setLibelle(resultat.getString(2));
                offreService.setDate(resultat.getDate(3));
                offreService.setDescription(resultat.getString(4));
                offreService.setMembre(membreDao.selectById(resultat.getInt(5)));
                offreService.setCategorieService(categorieServiceDao.selectById(resultat.getInt(6)));
                offreService.setService(serviceDao.selectById(resultat.getInt(7)));
                offreService.setEtat(resultat.getString(8));
                offreServices.add(offreService);
            }
            return offreServices;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement" + ex.getMessage());
            return null;
        }
    }

    @Override
    public List<OffreService> selectByService(Service service) {
         String req = "select * from offre_service where idservice=?";
            MembreDao membreDao = new MembreDao();
            CategorieServiceDao categorieServiceDao = new CategorieServiceDao();
            ServiceDao serviceDao = new ServiceDao();
            List<OffreService> offreServices = new ArrayList<>();
        try {
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, service.getId());
            ResultSet resultat = ps.executeQuery();
           

            while (resultat.next()) {
                OffreService offreService = new OffreService();
                offreService.setId(resultat.getInt(1));
                offreService.setLibelle(resultat.getString(2));
                offreService.setDate(resultat.getDate(3));
                offreService.setDescription(resultat.getString(4));
                offreService.setMembre(membreDao.selectById(resultat.getInt(5)));
                offreService.setCategorieService(categorieServiceDao.selectById(resultat.getInt(6)));
                offreService.setService(serviceDao.selectById(resultat.getInt(7)));
                offreService.setEtat(resultat.getString(8));
                offreServices.add(offreService);
            }
            return offreServices;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement" + ex.getMessage());
            return null;
        }
    }
    
}
