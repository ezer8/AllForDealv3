/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.esprit.pidev.dao.classes;


import edu.esprit.pidev.dao.interfaces.IProduitDao;
import edu.esprit.pidev.entities.CategorieProduit;
import edu.esprit.pidev.entities.CategorieService;
import edu.esprit.pidev.entities.Membre;
import edu.esprit.pidev.entities.Produit;
import edu.esprit.pidev.technique.DataSource;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Asus
 */




public class ProduitDao implements IProduitDao <Produit,Membre,CategorieProduit> {
    
    private final Connection connection;

    public ProduitDao() {
        connection = DataSource.getInstance().getConnection();
    }

    @Override
    public int add(Produit produit) {
        try {
            String req = "insert into produit (libelle,description,date,prix,etat,image,idcategorie,idmembre) values (?,?,?,?,?,?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(req);
            
            ps.setString(1, produit.getLibelle());
            ps.setString(2, produit.getDescription());
            ps.setDate(3, new java.sql.Date(produit.getDate().getTime()));
            ps.setFloat(4, produit.getPrix());
            ps.setString(5, produit.getEtat());
            ps.setString(6, produit.getImage());
            ps.setInt(7, produit.getCategorie().getId());
            ps.setInt(8, produit.getMembre().getId());
            ps.setInt(9,produit.getNbVente());
            
           return  ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProduitDao.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    @Override
    public int update(Produit produit) {
        try {
            String req = "update produit set libelle=?,description=?,date=?,prix=?,etat=?,image=?,idcategorie=?,idmembre=?,nb_vente=? where id=?";
            PreparedStatement ps = connection.prepareStatement(req);
            
            ps.setString(1, produit.getLibelle());
            ps.setString(2, produit.getDescription());
            ps.setDate(3, new java.sql.Date(produit.getDate().getTime()));
            ps.setFloat(4, produit.getPrix());
            ps.setString(5, produit.getEtat());
            ps.setString(6, produit.getImage());
            ps.setInt(7, produit.getCategorie().getId());
            ps.setInt(8, produit.getMembre().getId());
            ps.setInt(9, produit.getNbVente());
            ps.setInt(10, produit.getId());
            
           return  ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProduitDao.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    @Override
    public int delete(Produit produit) {
        try {
            String req = "delete from produit where id=?";
            PreparedStatement ps = connection.prepareStatement(req);
            
            ps.setInt(1, produit.getId());
            
           return  ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProduitDao.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    @Override
    public Produit selectById(int id) {
        Produit produit = new Produit();
        CategorieProduitDao categorieProduitDao=new CategorieProduitDao();
        MembreDao membreDao=new MembreDao();
        String requete = "select * from produit where id=?";
        try {
            PreparedStatement ps = connection.prepareStatement(requete);
            ps.setInt(1, id);
            ResultSet resultat = ps.executeQuery();
            while (resultat.next()) {
                produit.setId(resultat.getInt(1));
                
                produit.setLibelle(resultat.getString(2));
                produit.setDescription(resultat.getString(3));
                produit.setDate(resultat.getDate(4));
                produit.setPrix(resultat.getFloat(5));
                produit.setEtat(resultat.getString(6));
                produit.setImage(resultat.getString(7));
                produit.setCategorie(categorieProduitDao.selectById(resultat.getInt(8)));
                produit.setMembre(membreDao.selectById(resultat.getInt(9)));
                produit.setNbVente(resultat.getInt(10));
            }
            return produit;

        } catch (SQLException ex) {
            System.out.println("erreur lors de la recherche du depot " + ex.getMessage());
            return null;
        }
    }

   

    @Override
    public List<Produit> selectByUser(Membre user) {
        List<Produit> listeProduit=new ArrayList<>();
        CategorieProduitDao categorieProduitDao=new CategorieProduitDao();
        MembreDao membreDao=new MembreDao();
        String requete = "select * from produit where idmembre=?";
        try {
            PreparedStatement ps = connection.prepareStatement(requete);
            //ps.setInt(1, user.getId());
            ResultSet resultat = ps.executeQuery();
            while (resultat.next()) {
                Produit produit=new Produit();
                produit.setId(resultat.getInt(1));
                
                produit.setLibelle(resultat.getString(2));
                produit.setDescription(resultat.getString(3));
                produit.setDate(resultat.getDate(4));
                produit.setPrix(resultat.getFloat(5));
                produit.setEtat(resultat.getString(6));
                produit.setImage(resultat.getString(7));
                produit.setCategorie(categorieProduitDao.selectById(resultat.getInt(8)));
                produit.setMembre(membreDao.selectById(resultat.getInt(9)));
                produit.setNbVente(resultat.getInt(10));
                listeProduit.add(produit);
            }
            return listeProduit;

        } catch (SQLException ex) {
            System.out.println("erreur lors de la recherche du depot " + ex.getMessage());
            return null;
        }
    }

    @Override
    public List<Produit> selectAll() {
        List<Produit> listeProduit=new ArrayList<>();
        CategorieProduitDao categorieProduitDao=new CategorieProduitDao();
        MembreDao membreDao=new MembreDao();
        String requete = "select * from produit";
        try {
            PreparedStatement ps = connection.prepareStatement(requete);
            ResultSet resultat = ps.executeQuery();
            while (resultat.next()) {
                Produit produit=new Produit();
                produit.setId(resultat.getInt(1));
                
                produit.setLibelle(resultat.getString(2));
                produit.setDescription(resultat.getString(3));
                produit.setDate(resultat.getDate(4));
                produit.setPrix(resultat.getFloat(5));
                produit.setEtat(resultat.getString(6));
                produit.setImage(resultat.getString(7));
                produit.setCategorie(categorieProduitDao.selectById(resultat.getInt(8)));
                produit.setMembre(membreDao.selectById(resultat.getInt(9)));
                produit.setNbVente(resultat.getInt(10));
                listeProduit.add(produit);
            }
            return listeProduit;

        } catch (SQLException ex) {
            System.out.println("erreur lors de la recherche du depot " + ex.getMessage());
            return null;
        }
    }

    @Override
    public List<Produit> selectByLibelle(String libelle) {
        List<Produit> listeProduit=new ArrayList<>();
        CategorieProduitDao categorieProduitDao=new CategorieProduitDao();
        MembreDao membreDao=new MembreDao();
        String requete = "select * from produit where libelle=?";
        try {
            PreparedStatement ps = connection.prepareStatement(requete);
            ResultSet resultat = ps.executeQuery();
            ps.setString(1, libelle);
            while (resultat.next()) {
                Produit produit=new Produit();
                produit.setId(resultat.getInt(1));
                
                produit.setLibelle(resultat.getString(2));
                produit.setDescription(resultat.getString(3));
                produit.setDate(resultat.getDate(4));
                produit.setPrix(resultat.getFloat(5));
                produit.setEtat(resultat.getString(6));
                produit.setImage(resultat.getString(7));
                produit.setCategorie(categorieProduitDao.selectById(resultat.getInt(8)));
                produit.setMembre(membreDao.selectById(resultat.getInt(9)));
                produit.setNbVente(resultat.getInt(10));
                listeProduit.add(produit);
            }
            return listeProduit;

        } catch (SQLException ex) {
            System.out.println("erreur lors de la recherche du depot " + ex.getMessage());
            return null;
        }
    }

    @Override
    public List<Produit> selectByCategorie(CategorieProduit categorie) {
        List<Produit> listeProduit=new ArrayList<>();
        CategorieProduitDao categorieProduitDao=new CategorieProduitDao();
        MembreDao membreDao=new MembreDao();
        String requete = "select * from produit where idcategorie=?";
        try {
            PreparedStatement ps = connection.prepareStatement(requete);
            ResultSet resultat = ps.executeQuery();
            ps.setInt(1, categorie.getId());
            while (resultat.next()) {
                Produit produit=new Produit();
                produit.setId(resultat.getInt(1));
                
                produit.setLibelle(resultat.getString(2));
                produit.setDescription(resultat.getString(3));
                produit.setDate(resultat.getDate(4));
                produit.setPrix(resultat.getFloat(5));
                produit.setEtat(resultat.getString(6));
                produit.setImage(resultat.getString(7));
                produit.setCategorie(categorieProduitDao.selectById(resultat.getInt(8)));
                produit.setMembre(membreDao.selectById(resultat.getInt(9)));
                produit.setNbVente(resultat.getInt(10));
                listeProduit.add(produit);
            }
            return listeProduit;

        } catch (SQLException ex) {
            System.out.println("erreur lors de la recherche du depot " + ex.getMessage());
            return null;
        }
    }

    @Override
    public List<Produit> selectByDate(Date date) {
        List<Produit> listeProduit=new ArrayList<>();
        CategorieProduitDao categorieProduitDao=new CategorieProduitDao();
        MembreDao membreDao=new MembreDao();
        String requete = "select * from produit where date=?";
        try {
            PreparedStatement ps = connection.prepareStatement(requete);
            ResultSet resultat = ps.executeQuery();
            ps.setDate(1, date);
            while (resultat.next()) {
                Produit produit=new Produit();
                produit.setId(resultat.getInt(1));
                
                produit.setLibelle(resultat.getString(3));
                produit.setDescription(resultat.getString(4));
                produit.setDate(resultat.getDate(5));
                produit.setPrix(resultat.getFloat(6));
                produit.setEtat(resultat.getString(7));
                produit.setImage(resultat.getString(8));
                produit.setCategorie(categorieProduitDao.selectById(resultat.getInt(8)));
                produit.setMembre(membreDao.selectById(resultat.getInt(9)));
                produit.setNbVente(resultat.getInt(10));
                listeProduit.add(produit);
            }
            return listeProduit;

        } catch (SQLException ex) {
            System.out.println("erreur lors de la recherche du depot " + ex.getMessage());
            return null;
        }
    }

    @Override
    public List<Produit> selectByPrix(float prix) {
        List<Produit> listeProduit=new ArrayList<>();
        CategorieProduitDao categorieProduitDao=new CategorieProduitDao();
        MembreDao membreDao=new MembreDao();
        String requete = "select * from produit where prix=?";
        try {
            PreparedStatement ps = connection.prepareStatement(requete);
            ResultSet resultat = ps.executeQuery();
            ps.setFloat(1, prix);
            while (resultat.next()) {
                Produit produit=new Produit();
                produit.setId(resultat.getInt(1));
                
                produit.setLibelle(resultat.getString(2));
                produit.setDescription(resultat.getString(3));
                produit.setDate(resultat.getDate(4));
                produit.setPrix(resultat.getFloat(5));
                produit.setEtat(resultat.getString(6));
                produit.setImage(resultat.getString(7));
                produit.setCategorie(categorieProduitDao.selectById(resultat.getInt(8)));
                produit.setMembre(membreDao.selectById(resultat.getInt(9)));
                produit.setNbVente(resultat.getInt(10));
                listeProduit.add(produit);
            }
            return listeProduit;

        } catch (SQLException ex) {
            System.out.println("erreur lors de la recherche du depot " + ex.getMessage());
            return null;
        }
    }

    @Override
    public List<Produit> selectByPrixSup(float prix) {
        List<Produit> listeProduit=new ArrayList<>();
        CategorieProduitDao categorieProduitDao=new CategorieProduitDao();
        MembreDao membreDao=new MembreDao();
        String requete = "select * from produit where prix >= ?";
        try {
            PreparedStatement ps = connection.prepareStatement(requete);
            ResultSet resultat = ps.executeQuery();
            ps.setFloat(1, prix);
            while (resultat.next()) {
                Produit produit=new Produit();
                produit.setId(resultat.getInt(1));
                
                produit.setLibelle(resultat.getString(2));
                produit.setDescription(resultat.getString(3));
                produit.setDate(resultat.getDate(4));
                produit.setPrix(resultat.getFloat(5));
                produit.setEtat(resultat.getString(6));
                produit.setImage(resultat.getString(7));
                produit.setCategorie(categorieProduitDao.selectById(resultat.getInt(8)));
                produit.setMembre(membreDao.selectById(resultat.getInt(9)));
                produit.setNbVente(resultat.getInt(10));
                listeProduit.add(produit);
            }
            return listeProduit;

        } catch (SQLException ex) {
            System.out.println("erreur lors de la recherche du depot " + ex.getMessage());
            return null;
        }
    }

    @Override
    public List<Produit> selectByPrixInf(float prix) {
        List<Produit> listeProduit=new ArrayList<>();
        CategorieProduitDao categorieProduitDao=new CategorieProduitDao();
        MembreDao membreDao=new MembreDao();
        String requete = "select * from produit where prix <= ?";
        try {
            PreparedStatement ps = connection.prepareStatement(requete);
            ResultSet resultat = ps.executeQuery();
            ps.setFloat(1, prix);
            while (resultat.next()) {
                Produit produit=new Produit();
                produit.setId(resultat.getInt(1));
                
                produit.setLibelle(resultat.getString(2));
                produit.setDescription(resultat.getString(3));
                produit.setDate(resultat.getDate(4));
                produit.setPrix(resultat.getFloat(5));
                produit.setEtat(resultat.getString(6));
                produit.setImage(resultat.getString(7));
                produit.setCategorie(categorieProduitDao.selectById(resultat.getInt(8)));
                produit.setMembre(membreDao.selectById(resultat.getInt(9)));
                produit.setNbVente(resultat.getInt(10));
                listeProduit.add(produit);
            }
            return listeProduit;

        } catch (SQLException ex) {
            System.out.println("erreur lors de la recherche du depot " + ex.getMessage());
            return null;
        }
    }

    @Override
    public List<Produit> selectByEtat(String etat) {
        List<Produit> listeProduit=new ArrayList<>();
        CategorieProduitDao categorieProduitDao=new CategorieProduitDao();
        MembreDao membreDao=new MembreDao();
        String requete = "select * from produit where etat=?";
        try {
            PreparedStatement ps = connection.prepareStatement(requete);
            ps.setString(1, etat);
            ResultSet resultat = ps.executeQuery();
            
            while (resultat.next()) {
                Produit produit=new Produit();
                produit.setId(resultat.getInt(1));
                
                produit.setLibelle(resultat.getString(2));
                produit.setDescription(resultat.getString(3));
                produit.setDate(resultat.getDate(4));
                produit.setPrix(resultat.getFloat(5));
                produit.setEtat(resultat.getString(6));
                produit.setImage(resultat.getString(7));
                produit.setCategorie(categorieProduitDao.selectById(resultat.getInt(8)));
                produit.setMembre(membreDao.selectById(resultat.getInt(9)));
                produit.setNbVente(resultat.getInt(10));
                listeProduit.add(produit);
            }
            return listeProduit;

        } catch (SQLException ex) {
            System.out.println("erreur lors de la recherche du depot " + ex.getMessage());
            return null;
        }
    }

    @Override
    public Produit selectByID(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Produit selectByCode(String code) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Produit> selectTop(int limit) {
        String req = "SELECT * from produit order by `nb_vente` desc limit ?";
        CategorieProduitDao cpdao = new CategorieProduitDao();
        MembreDao mdao = new MembreDao();
        List<Produit> list = new ArrayList<>();
        try {
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, limit);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                Produit produit=new Produit();
                produit.setId(rs.getInt(1));
                produit.setLibelle(rs.getString(2));
                produit.setDescription(rs.getString(3));
                produit.setDate(rs.getDate(4));
                produit.setPrix(rs.getFloat(5));
                produit.setEtat(rs.getString(6));
                produit.setImage(rs.getString(7));
                produit.setCategorie(cpdao.selectById(rs.getInt(8)));
                produit.setMembre(mdao.selectById(rs.getInt(9)));
                produit.setNbVente(rs.getInt(10));
                list.add(produit);
            }
        } catch (SQLException ex) {
            System.out.println("error :"+ex.getMessage());
        }
        return list;
    }
    
}
