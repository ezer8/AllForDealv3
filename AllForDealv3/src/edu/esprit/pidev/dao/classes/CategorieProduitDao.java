/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.esprit.pidev.dao.classes;

import edu.esprit.pidev.dao.interfaces.ICategorieDao;
import edu.esprit.pidev.entities.CategorieProduit;
import edu.esprit.pidev.entities.Produit;
import edu.esprit.pidev.technique.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Asus
 */
public class CategorieProduitDao implements ICategorieDao<CategorieProduit>{
    
    private Connection connection;

    public CategorieProduitDao() {
        connection = DataSource.getInstance().getConnection();
    }

    @Override
    public int add(CategorieProduit categorie) {
        try {
            String req = "insert into categorie_produit (libelle) values (?)";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setString(1, categorie.getLibelle());
            
            
           return  ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProduitDao.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    @Override
    public int update(CategorieProduit categorie) {
        try {
            String req = "update categorie_produit set libelle=? where id=?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setString(1, categorie.getLibelle());
            ps.setInt(2, categorie.getId());
            
            
           return  ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProduitDao.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    @Override
    public int delete(CategorieProduit categorie) {
        try {
            String req = "delete from categorie_produit where id=?";
            PreparedStatement ps = connection.prepareStatement(req);
            
            ps.setInt(1, categorie.getId());
            
           return  ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProduitDao.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    @Override
    public List<CategorieProduit> selectAll() {
        List<CategorieProduit> listeCategorieProduit=new ArrayList<>();
       if (connection ==null){
           System.out.println("conn null");
       }
           
       
        String requete = "select * from categorie_produit";
        try {
            PreparedStatement ps = connection.prepareStatement(requete);
            ResultSet resultat = ps.executeQuery();
            while (resultat.next()) {
                CategorieProduit categorieProduit=new CategorieProduit();
                categorieProduit.setId(resultat.getInt(1));
                categorieProduit.setLibelle(resultat.getString(2));
                
                listeCategorieProduit.add(categorieProduit);
            }
            return listeCategorieProduit;

        } catch (SQLException ex) {
            System.out.println("erreur lors de la recherche du depot " + ex.getMessage());
            return null;
        }
    }

    @Override
    public CategorieProduit selectById(int id) {
        CategorieProduit categorieProduit = new CategorieProduit();
     
        String requete = "select * from categorie_produit where id=?";
        try {
            PreparedStatement ps = connection.prepareStatement(requete);
            ps.setInt(1, id);
            ResultSet resultat = ps.executeQuery();
            while (resultat.next()) {
                categorieProduit.setId(resultat.getInt(1));
                categorieProduit.setLibelle(resultat.getString(2));
                
                
            }
            return categorieProduit;

        } catch (SQLException ex) {
            System.out.println("erreur lors de la recherche du depot " + ex.getMessage());
            return null;
        }
    }

    @Override
    public List<CategorieProduit> selectByLibelle(String libelle) {
        List<CategorieProduit> listeCategorieProduit=new ArrayList<>();
      
        String requete = "select * from categorie_produit where libelle=?";
        try {
            PreparedStatement ps = connection.prepareStatement(requete);
            ResultSet resultat = ps.executeQuery();
            ps.setString(1, libelle);
            while (resultat.next()) {
                CategorieProduit categorieProduit=new CategorieProduit();
                categorieProduit.setId(resultat.getInt(1));
                categorieProduit.setLibelle(resultat.getString(2));
                
                listeCategorieProduit.add(categorieProduit);
            }
            return listeCategorieProduit;

        } catch (SQLException ex) {
            System.out.println("erreur lors de la recherche du depot " + ex.getMessage());
            return null;
        }
    }

    
    
}
