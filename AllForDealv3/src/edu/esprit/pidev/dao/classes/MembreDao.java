/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.esprit.pidev.dao.classes;

import edu.esprit.pidev.dao.interfaces.IMembreDao;
import edu.esprit.pidev.entities.Membre;
import edu.esprit.pidev.technique.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Asus
 */
public class MembreDao implements IMembreDao<Membre> {

    Connection connection;

    public MembreDao() {
        connection = DataSource.getInstance().getConnection();
    }

    @Override
    public int add(Membre membre) {
        int i = 1;
        try {
            String request = "insert into membre values(null,?,?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(request);
            ps.setString(1, membre.getLogin());
            ps.setString(2, membre.getPassword());
            ps.setString(3, membre.getNom());
            ps.setString(4, membre.getPrenom());
            ps.setInt(5, membre.getAge());
            ps.setString(6, membre.getEmail());
            ps.setString(7, membre.getNumeroTel());
            ps.setString(8, membre.getAdresse());
            ps.setInt(9, membre.getBonus());
            ps.setObject(10, membre.getDateVisite());
            ps.setString(11, membre.getType());
            ps.setInt(12,membre.getNbProdVendu());
            i = ps.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("erreur: " + ex.getMessage());

        }
        return i;
    }
    public int add2(Membre membre) {
        int i = 1;
        try {
            String request = "insert into membre values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(request);
            ps.setInt(1, membre.getId());
            ps.setString(2, membre.getLogin());
            ps.setString(3, membre.getPassword());
            ps.setString(4, membre.getNom());
            ps.setString(5, membre.getPrenom());
            ps.setInt(6, membre.getAge());
            ps.setString(7, membre.getEmail());
            ps.setString(8, membre.getNumeroTel());
            ps.setString(9, membre.getAdresse());
            ps.setInt(10, membre.getBonus());
            ps.setObject(11, membre.getDateVisite());
            ps.setString(12, membre.getType());
            ps.setInt(13,membre.getNbProdVendu());
            i = ps.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("erreur: " + ex.getMessage());

        }
        return i;
    }

    @Override
    public int update(Membre membre) {
        int i = 0;
        try {
            String request = "update membre set login= ? , mdp = ? ,nom = ?,prenom = ?,age = ?,email = ?,numerotel = ?,adresse = ?,bonus=?,dvisite=?,type=?,nb_prod_vendu=? where id = ?";
            PreparedStatement ps = connection.prepareStatement(request);
            ps.setString(1, membre.getLogin());
            ps.setString(2, membre.getPassword());
            ps.setString(3, membre.getNom());
            ps.setString(4, membre.getPrenom());
            ps.setInt(5, membre.getAge());
            ps.setString(6, membre.getEmail());
            ps.setString(7, membre.getNumeroTel());
            ps.setString(8, membre.getAdresse());
            ps.setInt(9, membre.getBonus());
            ps.setObject(10, membre.getDateVisite());
            ps.setString(11, membre.getType());
            ps.setInt(12,membre.getNbProdVendu());
            ps.setInt(13, membre.getId());
            i = ps.executeUpdate();
        } catch (SQLException ex) {

        }
        return i;
    }

    @Override
    public int delete(Membre membre) {
        int i = 0;
        try {
            String request = "delete from membre where id = ? ";
            PreparedStatement ps = connection.prepareStatement(request);
            ps.setInt(1, membre.getId());
            i = ps.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("error:"+ex.getMessage());
        }
        return i;
    }

    @Override
    public List<Membre> selectAll() {
        List<Membre> list = new ArrayList<>();
        try {
            String request = "select * from membre";
            PreparedStatement ps = connection.prepareStatement(request);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Membre m = new Membre(rs.getString("login"), rs.getString("mdp"), rs.getString("nom"), rs.getString("prenom"), rs.getInt("age"), rs.getString("email"), rs.getString("numeroTel"),
                        rs.getString("adresse"), rs.getInt("bonus"), rs.getDate("dvisite"), rs.getString("type"),rs.getInt("nb_prod_vendu"));
                m.setId(rs.getInt("id"));
                
                list.add(m);
            }
        } catch (SQLException ex) {
            System.out.println("error:"+ex.getMessage());
        }
        return list;
    }

    @Override
    public Membre selectById(int id) {
        Membre m = null;
        try {
            String request = "select * from membre where id=?";
            PreparedStatement ps = connection.prepareStatement(request);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                m = new Membre(rs.getString("login"), rs.getString("mdp"), rs.getString("nom"), rs.getString("prenom"), rs.getInt("age"), rs.getString("email"), rs.getString("numeroTel"),
                        rs.getString("adresse"), rs.getInt("bonus"), rs.getDate("dvisite"), rs.getString("type"),rs.getInt("nb_prod_vendu"));
                m.setId(rs.getInt("id"));
            }
        } catch (SQLException ex) {
            System.out.println("error:"+ex.getMessage());
        }
        return m;
    }

    @Override
    public List<Membre> selectBy(String filtre, String valeur) {
        List<Membre> list = new ArrayList<>();
        try {
            String request = "select * from membre where ?=?";
            PreparedStatement ps = connection.prepareStatement(request);
            ps.setString(1, filtre);
            ps.setString(2, valeur);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Membre m = new Membre(rs.getString("login"), rs.getString("mdp"), rs.getString("nom"), rs.getString("prenom"), rs.getInt("age"), rs.getString("email"), rs.getString("numeroTel"),
                        rs.getString("adresse"), rs.getInt("bonus"), rs.getDate("dvisite"), rs.getString("type"),rs.getInt("nb_prod_vendu"));
                m.setId(rs.getInt("id"));
                list.add(m);
            }
        } catch (SQLException ex) {
            System.out.println("error:"+ex.getMessage());
        }
        return list;
    }

    @Override
    public Membre selectByLoginPass(String login, String pass) {
        Membre m = null;
        try {
            String request = "select * from membre where login = ? AND mdp = ?";
            PreparedStatement ps = connection.prepareStatement(request);
            ps.setString(1, login);
            ps.setString(2, pass);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                m = new Membre(rs.getString("login"), rs.getString("mdp"), rs.getString("nom"), rs.getString("prenom"), rs.getInt("age"), rs.getString("email"), rs.getString("numeroTel"),
                        rs.getString("adresse"), rs.getInt("bonus"), rs.getDate("dvisite"), rs.getString("type"),rs.getInt("nb_prod_vendu"));
                m.setId(rs.getInt("id"));
                
            }
        } catch (SQLException ex) {
            System.out.println("error:"+ex.getMessage());
        }
        return m;
    }

    @Override
    public List<Membre> selectTop(int limit) {
        List<Membre> list = new ArrayList<>();
        try {
            String request = "select * from membre order by nb_prod_vendu desc limit ?";
            PreparedStatement ps = connection.prepareStatement(request);
            ps.setInt(1,limit);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Membre m = new Membre(rs.getString("login"), rs.getString("mdp"), rs.getString("nom"), rs.getString("prenom"), rs.getInt("age"), rs.getString("email"), rs.getString("numeroTel"),
                        rs.getString("adresse"), rs.getInt("bonus"), rs.getDate("dvisite"), rs.getString("type"),rs.getInt("nb_prod_vendu"));
                m.setId(rs.getInt("id"));
                
                list.add(m);
            }
        } catch (SQLException ex) {
            System.out.println("error:"+ex.getMessage());
        }
        return list;
    }
    @Override
    public Membre Authentification(String pseudo, String mdp) {
        Membre m = null;
          try {
              String request = "select * from membre where login=? and mdp=?";
              PreparedStatement ps = connection.prepareStatement(request);
              ps.setString(1, pseudo);
              ps.setString(2, mdp);
              ResultSet rs = ps.executeQuery();
                  while (rs.next()) {
              m = new Membre(rs.getString("login"), rs.getString("mdp"), rs.getString("nom"), rs.getString("prenom"), rs.getInt("age"), rs.getString("email"), rs.getString("numeroTel"),
              rs.getString("adresse"), rs.getInt("bonus"), rs.getDate("dvisite"), rs.getString("type"),rs.getInt("nb_prod_vendu"));}
          } catch (SQLException ex) {
              System.out.println("error :"+ex.getMessage());
          }
        return m;
    }

}
