/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.esprit.pidev.dao.classes;

import edu.esprit.pidev.dao.interfaces.IServiceDao;
import edu.esprit.pidev.entities.CategorieService;
import edu.esprit.pidev.entities.Membre;
import edu.esprit.pidev.entities.Service;
import edu.esprit.pidev.technique.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Asus
 */
public class ServiceDao implements IServiceDao<Service, Membre , CategorieService> {

    private Connection connection;

    public ServiceDao() {
        connection = DataSource.getInstance().getConnection();
    }

    @Override
    public int add(Service service) {
        try {
             
            String req = "insert into service (libelle,gouvernorat,date,description,idmembre,idcategorie) values (?,?,?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setString(1, service.getLibelle());
            ps.setString(2, service.getGouvernorat());
            ps.setDate(3, service.getDate());
            ps.setString(4, service.getDescription());
            ps.setInt(5, service.getMembre().getId());
           
            ps.setInt(6, service.getCategorieService().getId());
            return ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ServiceDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    @Override
    public int update(Service service) {
        try {
            String req = "update service set libelle = ?,gouvernorat = ?,date = ? ,description = ?,idmembre = ?,idcategorie = ? where id = ?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setString(1, service.getLibelle());
            ps.setString(2, service.getGouvernorat());
            ps.setDate(3, service.getDate());
            ps.setString(4, service.getDescription());
            ps.setInt(5, service.getMembre().getId());
            System.out.println(service.getCategorieService());
            ps.setInt(6, service.getCategorieService().getId());
            ps.setInt(7, service.getId());
            return ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ServiceDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    @Override
    public int delete(Service service) {
        try {
            String req = "delete from service where id=?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, service.getId());
            return ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ServiceDao.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }

    }

    @Override
    public Service selectById(int id) {
        String req = "select * from service where id=?";
            MembreDao membreDao = new MembreDao();
            CategorieServiceDao categorieServiceDao = new CategorieServiceDao();
            Service service = new Service();
        try {
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ResultSet resultat = ps.executeQuery();
           

            while (resultat.next()) {

                service.setId(resultat.getInt(1));
                service.setLibelle(resultat.getString(2));
                service.setGouvernorat(resultat.getString(3));
                service.setDate(resultat.getDate(4));
                service.setDescription(resultat.getString(5));
                service.setMembre(membreDao.selectById(resultat.getInt(6)));
                service.setCategorieService(categorieServiceDao.selectById(resultat.getInt(7)));

            }
            return service;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement" + ex.getMessage());
            return null;
        }
    }

  

    @Override
    public List<Service> selectByUser(Membre user) {
  String req = "select * from service where idmembre=?";
      MembreDao membreDao = new MembreDao();
      CategorieServiceDao categorieServiceDao = new CategorieServiceDao();
      List<Service> services = new ArrayList();

        try {
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, user.getId());
            ResultSet resultat = ps.executeQuery();
           

            while (resultat.next()) {
                Service service = new Service();
                service.setId(resultat.getInt(1));
                service.setLibelle(resultat.getString(2));
                service.setGouvernorat(resultat.getString(3));
                service.setDate(resultat.getDate(4));
                service.setDescription(resultat.getString(5));
                service.setMembre(membreDao.selectById(resultat.getInt(6)));
                service.setCategorieService(categorieServiceDao.selectById(resultat.getInt(7)));
                services.add(service);

            }
            return services;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement" + ex.getMessage());
            return null;
        }
    }

    @Override
    public List<Service> selectByCategorie(CategorieService categorie) {
      String req = "select * from service where idcategorie=?";
      MembreDao membreDao = new MembreDao();
      CategorieServiceDao categorieServiceDao = new CategorieServiceDao();
      List<Service> services = new ArrayList();

        try {
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, categorie.getId());
            ResultSet resultat = ps.executeQuery();
           

            while (resultat.next()) {
                Service service = new Service();
                service.setId(resultat.getInt(1));
                service.setLibelle(resultat.getString(2));
                service.setGouvernorat(resultat.getString(3));
                service.setDate(resultat.getDate(4));
                service.setDescription(resultat.getString(5));
                service.setMembre(membreDao.selectById(resultat.getInt(6)));
                service.setCategorieService(categorieServiceDao.selectById(resultat.getInt(7)));
                services.add(service);

            }
            return services;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement" + ex.getMessage());
            return null;
        }
    }

    @Override
    public List<Service> selectAll() {
       String req = "select * from service ";
       MembreDao membreDao = new MembreDao();
       CategorieServiceDao categorieServiceDao = new CategorieServiceDao();
       List<Service> services = new ArrayList(); 

        try {
            PreparedStatement ps = connection.prepareStatement(req);
            ResultSet resultat = ps.executeQuery();
            

            while (resultat.next()) {
                Service service = new Service();
                service.setId(resultat.getInt(1));
                service.setLibelle(resultat.getString(2));
                service.setGouvernorat(resultat.getString(3));
                service.setDate(resultat.getDate(4));
                service.setDescription(resultat.getString(5));
                service.setMembre(membreDao.selectById(resultat.getInt(6)));
                service.setCategorieService(categorieServiceDao.selectById(resultat.getInt(7)));
                services.add(service);

            }
            return services;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement" + ex.getMessage());
            return null;
        }
    }

    @Override
    public List<Service> selectByLibelle(String libelle) {
       String req = "select * from service where libelle=?";
       MembreDao membreDao = new MembreDao();
       CategorieServiceDao categorieServiceDao = new CategorieServiceDao();
       List<Service> services = new ArrayList();

        try {
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setString(1, libelle);
            ResultSet resultat = ps.executeQuery();
           

            while (resultat.next()) {
                Service service = new Service();
                service.setId(resultat.getInt(1));
                service.setLibelle(resultat.getString(2));
                service.setGouvernorat(resultat.getString(3));
                service.setDate(resultat.getDate(4));
                service.setDescription(resultat.getString(5));
                service.setMembre(membreDao.selectById(resultat.getInt(6)));
                service.setCategorieService(categorieServiceDao.selectById(resultat.getInt(7)));
                services.add(service);

            }
            return services;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement" + ex.getMessage());
            return null;
        }
    }

    @Override
    public List<Service> selectByDate(Date date) {
        String req = "select * from service where date=?";
       MembreDao membreDao = new MembreDao();
       CategorieServiceDao categorieServiceDao = new CategorieServiceDao();
       List<Service> services = new ArrayList();

        try {
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setDate(1, date);
            ResultSet resultat = ps.executeQuery();
           

            while (resultat.next()) {
                Service service = new Service();
                service.setId(resultat.getInt(1));
                service.setLibelle(resultat.getString(2));
                service.setGouvernorat(resultat.getString(3));
                service.setDate(resultat.getDate(4));
                service.setDescription(resultat.getString(5));
                service.setMembre(membreDao.selectById(resultat.getInt(6)));
                service.setCategorieService(categorieServiceDao.selectById(resultat.getInt(7)));
                services.add(service);

            }
            return services;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement" + ex.getMessage());
            return null;
        }
    }

}
