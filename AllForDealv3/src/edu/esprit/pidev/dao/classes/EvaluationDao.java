/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.esprit.pidev.dao.classes;

import edu.esprit.pidev.dao.interfaces.IEvaluationDao;
import edu.esprit.pidev.entities.Evaluation;
import edu.esprit.pidev.entities.Membre;
import edu.esprit.pidev.entities.Produit;
import edu.esprit.pidev.entities.Service;
import edu.esprit.pidev.technique.DataSource;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Asus
 */
public class EvaluationDao implements IEvaluationDao<Evaluation, Membre, Produit, Service>{

       private Connection connection;

    public EvaluationDao() {
        connection = DataSource.getInstance().getConnection();
    }

    @Override
     public int add(Evaluation evaluation) {
        try {
            String req = "insert into Evaluation (evaluation,commentaire,date,idproduit,idservice,idmembre) values (?,?,?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, evaluation.getEvaluation());
             ps.setString(2, evaluation.getCommentaire());
            ps.setDate(3, evaluation.getDate());
            ps.setInt(4, evaluation.getProduit().getId());
           ps.setInt(5, evaluation.getService().getId());
          ps.setInt(6, evaluation.getMembre().getId());
            return ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ServiceDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    @Override
    public int update(Evaluation evaluation) {
        try {
              String req = "update Evaluation set evaluation = ?,commentaire = ? ,date = ?,idproduit = ?,idservice = ?,idmembre = ? where id = ?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, evaluation.getEvaluation());
             ps.setString(2, evaluation.getCommentaire());
            ps.setDate(3, evaluation.getDate());
            ps.setInt(4, evaluation.getProduit().getId());
           ps.setInt(5, evaluation.getService().getId());
          ps.setInt(6, evaluation.getMembre().getId());
          ps.setInt(6, evaluation.getId());
            return ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ServiceDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    
    @Override
     public int delete(Evaluation evaluation) {
        try {
            String req = "delete from evaluation where id=?";
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, evaluation.getId());
            return ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ServiceDao.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }

    }
    @Override
    public List<Evaluation> selectAll() {
         String req = "select * from evaluation ";
            MembreDao membreDao = new MembreDao();
            ProduitDao produitDao = new ProduitDao();
            ServiceDao serviceDao = new ServiceDao();
           
            List<Evaluation> evaluations = new ArrayList<>();
        try {
            PreparedStatement ps = connection.prepareStatement(req);
           
            ResultSet resultat = ps.executeQuery();
           

            while (resultat.next()) {
                Evaluation evaluation = new Evaluation();
                evaluation.setId(resultat.getInt(1));
                evaluation.setEvaluation(resultat.getInt(2));
                evaluation.setCommentaire(resultat.getString(3));
                evaluation.setDate(resultat.getDate(4));
                evaluation.setProduit(produitDao.selectById(resultat.getInt(5)));
               evaluation.setService(serviceDao.selectById(resultat.getInt(6)));
               evaluation.setMembre(membreDao.selectById(resultat.getInt(7)));
               evaluations.add(evaluation);

            }
            return evaluations;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement" + ex.getMessage());
            return null;
        }
    }

    @Override
    public Evaluation selectByUserProduit(Membre user, Produit produit) {
        String req = "select * from evaluation where idmembre=? AND idproduit =?";
            MembreDao membreDao = new MembreDao();
            ProduitDao produitDao = new ProduitDao();
            ServiceDao serviceDao = new ServiceDao();
            Evaluation evaluation = new Evaluation();
            
        try {
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, user.getId());
            ps.setInt(2, produit.getId());
            ResultSet resultat = ps.executeQuery();
           

            while (resultat.next()) {

                evaluation.setId(resultat.getInt(1));
                evaluation.setEvaluation(resultat.getInt(2));
                evaluation.setCommentaire(resultat.getString(3));
                evaluation.setDate(resultat.getDate(4));
                evaluation.setProduit(produitDao.selectById(resultat.getInt(5)));
               evaluation.setService(serviceDao.selectById(resultat.getInt(6)));
               evaluation.setMembre(membreDao.selectById(resultat.getInt(7)));

            }
            return evaluation;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement" + ex.getMessage());
            return null;
        }
    }

    @Override
    public Evaluation selectById(int id) {
          String req = "select * from evaluation where id=?";
            MembreDao membreDao = new MembreDao();
            ProduitDao produitDao = new ProduitDao();
            ServiceDao serviceDao = new ServiceDao();
            Evaluation evaluation = new Evaluation();
        try {
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, id);
            ResultSet resultat = ps.executeQuery();
           

            while (resultat.next()) {

                evaluation.setId(resultat.getInt(1));
                evaluation.setEvaluation(resultat.getInt(2));
                evaluation.setCommentaire(resultat.getString(3));
                evaluation.setDate(resultat.getDate(4));
                evaluation.setProduit(produitDao.selectById(resultat.getInt(5)));
               evaluation.setService(serviceDao.selectById(resultat.getInt(6)));
               evaluation.setMembre(membreDao.selectById(resultat.getInt(7)));

            }
            return evaluation;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement" + ex.getMessage());
            return null;
        }
    }

    @Override
    public List<Evaluation> selectByProduit(Produit produit) {
       String req = "select * from evaluation where idproduit=?";
       
            MembreDao membreDao = new MembreDao();
            ProduitDao produitDao = new ProduitDao();
            ServiceDao serviceDao = new ServiceDao();
           
            List<Evaluation> evaluations = new ArrayList<>();
        try {
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, produit.getId());
            ResultSet resultat = ps.executeQuery();
           

            while (resultat.next()) {
              
                Evaluation evaluation = new Evaluation();
                evaluation.setId(resultat.getInt(1));
                evaluation.setEvaluation(resultat.getInt(2));
                evaluation.setCommentaire(resultat.getString(3));
                evaluation.setDate(resultat.getDate(4));
                evaluation.setProduit(produitDao.selectById(resultat.getInt(5)));
               evaluation.setService(serviceDao.selectById(resultat.getInt(6)));
               evaluation.setMembre(membreDao.selectById(resultat.getInt(7)));
               evaluations.add(evaluation);

            }
            return evaluations;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement" + ex.getMessage());
            return null;
        }
    }

    @Override
    public List<Evaluation> selectByService(Service service) {
      String req = "select * from evaluation where idservice=?";
            MembreDao membreDao = new MembreDao();
            ProduitDao produitDao = new ProduitDao();
            ServiceDao serviceDao = new ServiceDao();
           
            List<Evaluation> evaluations = new ArrayList<>();
        try {
            PreparedStatement ps = connection.prepareStatement(req);
            ps.setInt(1, service.getId());
            ResultSet resultat = ps.executeQuery();
           

            while (resultat.next()) {
                Evaluation evaluation = new Evaluation();
                evaluation.setId(resultat.getInt(1));
                evaluation.setEvaluation(resultat.getInt(2));
                evaluation.setCommentaire(resultat.getString(3));
                evaluation.setDate(resultat.getDate(4));
                evaluation.setProduit(produitDao.selectById(resultat.getInt(5)));
               evaluation.setService(serviceDao.selectById(resultat.getInt(6)));
               evaluation.setMembre(membreDao.selectById(resultat.getInt(7)));
               evaluations.add(evaluation);

            }
            return evaluations;
        } catch (SQLException ex) {
            System.out.println("erreur lors du chargement" + ex.getMessage());
            return null;
        }
    }
    
}
