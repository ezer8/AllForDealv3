/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.esprit.pidev.dao.interfaces;

import edu.esprit.pidev.dao.classes.*;
import java.sql.Date;
import java.util.List;

/**
 *
 * @author Asus
 */
public interface IProduitDao <T,K,X>{
    public int add(T produit);
    public int update(T produit);
    public int delete(T produit);
    
    public T selectByID(int id);
    public T selectByCode(String code);
    public List<T> selectByUser(K user);
    public List<T> selectAll();
    public T selectById(int id);
    public List<T> selectTop(int limit);
    public List<T> selectByLibelle(String libelle);
    public List<T> selectByCategorie(X categorie);
    public List<T> selectByDate(Date date); 
    public List<T> selectByPrix(float prix);
    public List<T> selectByPrixSup(float prix);
    public List<T> selectByPrixInf(float prix);
    public List<T> selectByEtat(String etat);
}
