/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.esprit.pidev.dao.interfaces;

import java.sql.Date;
import java.util.List;

/**
 *
 * @author Asus
 */
public interface IOffreServiceDao <T,K,C,S> {
    public int add(T offre);
    public int update(T offre);
    public int delete(T offre);
    public T selectById(int id);
    public T selectByCode(int code);
    public T selectByUser(K user);
    public List<T> selectByCategorie(C categorie);
    public List<T> selectAll();
    public List<T> selectByLibelle(String libelle);
    public List<T> selectByDate(Date date);
    public List<T> selectByEtat(String etat);
    public List<T> selectByService(S service);
}
