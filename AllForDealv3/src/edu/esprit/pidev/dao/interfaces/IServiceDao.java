/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.esprit.pidev.dao.interfaces;

import edu.esprit.pidev.dao.classes.*;
import java.sql.Date;
import java.util.List;

/**
 *
 * @author Asus
 */
public interface IServiceDao <T,K,C>{
    public int add(T service);
    public int update(T service);
    public int delete(T service);
    public T selectById(int id);
    public List<T> selectByUser(K user);
    public List<T> selectByCategorie(C categorie);
    public List<T> selectAll();
    public List<T> selectByLibelle(String libelle);
    public List<T> selectByDate(Date date);
    
}
