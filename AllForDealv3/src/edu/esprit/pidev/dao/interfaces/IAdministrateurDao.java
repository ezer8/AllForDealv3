/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.esprit.pidev.dao.interfaces;

import edu.esprit.pidev.dao.classes.*;
import java.util.List;

/**
 *
 * @author Asus
 */
public interface IAdministrateurDao<T> {
    public int add(T admin);
    public int update(T admin);
    public int delete(T admin);
    public List<T> selectAll();
}
