/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.esprit.pidev.dao.interfaces;

import java.util.List;

/**
 *
 * @author Asus
 */
public interface ITypeProduitDao <T,K> { 

    public int add(T type);
    public int update(T type);
    public int delete(T type);
    public List<T> selectAll();
    public List<T> selectByCategorie();

    
}
