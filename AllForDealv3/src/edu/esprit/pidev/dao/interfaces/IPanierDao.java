/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.esprit.pidev.dao.interfaces;

import java.util.List;

/**
 *
 * @author GhassenNasri
 */
public interface IPanierDao <T> {
    
    public int add(T panier);
    public int update(T panier);
    public int delete(T panier);
    public T selectById(int iduser);
    public List<T> selectAll();
    
}
