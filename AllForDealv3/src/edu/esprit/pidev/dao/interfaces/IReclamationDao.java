/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.esprit.pidev.dao.interfaces;

import edu.esprit.pidev.dao.classes.*;
import java.sql.Date;
import java.util.List;

/**
 *
 * @author Asus
 */
public interface IReclamationDao <T>{
     public int add(T reclamation);
    public int update(T reclamation);
    public int delete(T reclamation);
    public T selectById(int id);
    public List<T> selectAll();
}
