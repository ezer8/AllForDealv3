/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.esprit.pidev.dao.interfaces;

import java.util.List;

/**
 *
 * @author Asus
 * @param <T>
 */
public interface ICategorieDao <T>{
    public int add(T categorie);
    public int update(T categorie);
    public int delete(T categorie);
    public List<T> selectAll();
    public T selectById(int id);
    public List<T> selectByLibelle(String libelle);
}
