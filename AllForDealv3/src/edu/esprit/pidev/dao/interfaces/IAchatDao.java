/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.esprit.pidev.dao.interfaces;

import edu.esprit.pidev.dao.classes.*;
import java.sql.Date;
import java.util.List;

/**
 *
 * @author GhassenNasri
 */
public interface IAchatDao <T,K> {
    
    public int add(T achat);
    public int update(T achat);
    public int delete(T achat);
    public List<T> selectAll();
    public T selectByID(int id);
    public List<T> selectByDate(Date date);
    public List<T> selectByUser(K user);
    

}
