/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.esprit.pidev.dao.interfaces;

import edu.esprit.pidev.dao.classes.*;
import java.sql.Date;
import java.util.List;

/**
 *
 * @author Asus
 */
public interface IEvaluationDao <T,K,P,S>{
    public int add(T evaluation);
    public int update(T evaluation);
    public int delete(T evaluation);
    public List<T> selectAll();
    public T selectByUserProduit (K user, P Produit);
    public T selectById(int id);
    public List<T> selectByProduit (P produit);
    public List<T> selectByService (S service);
    
}
