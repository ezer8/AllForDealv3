/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.esprit.pidev.controllers;

import edu.esprit.pidev.vues.AccueilController;
import edu.esprit.pidev.vues.Pays;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.effect.BoxBlur;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author Asus
 */
public class PaysControl {
    Pays vue;
    double x,y;
    

    public PaysControl(Pays vue){
    this.vue = vue;
    }
    
    public void control(){
        AccueilController c = AccueilController.c;
        System.out.println("1");
        vue.getAriana().setStyle("-fx-background-color: transparent;-fx-border: none;");
        vue.getAriana().setOnMouseEntered(e -> vue.getL().setGraphic(new ImageView(vue.getArianai())));
        vue.getAriana().setOnAction(e -> {
            vue.getCombo().setValue("Ariana");
            vue.getWindow().close();
            c.getMarker().relocate(191, 9);
            c.getMarker().setVisible(true);
            System.out.println();
        });

        vue.getBeja1().setStyle("-fx-background-color: transparent;-fx-border: none;");
        vue.getBeja1().setOnMouseEntered(e -> vue.getL().setGraphic(new ImageView(vue.getBejai())));
        vue.getBeja1().setOnAction(e -> {
            vue.getCombo().setValue("Beja");
             vue.getWindow().close();
             c.getMarker().relocate(182, 12);
             c.getMarker().setVisible(true);
        });

        vue.getBeja2().setStyle("-fx-background-color: transparent;-fx-border: none;");
        vue.getBeja2().setOnMouseEntered(e -> vue.getL().setGraphic(new ImageView(vue.getBejai())));
        vue.getBeja2().setOnAction(e -> {
            vue.getCombo().setValue("Beja");
             vue.getWindow().close();
             c.getMarker().relocate(182, 12);
             c.getMarker().setVisible(true);
        });

        vue.getBenArous().setStyle("-fx-background-color: transparent;-fx-border: none;");
        vue.getBenArous().setOnMouseEntered(e -> vue.getL().setGraphic(new ImageView(vue.getBenArousi())));
        vue.getBenArous().setOnAction(e -> {
            vue.getCombo().setValue("Ben Arous");c.getMarker().relocate(195, 15);
             vue.getWindow().close();
             c.getMarker().setVisible(true);
        });

        vue.getBizerte().setStyle("-fx-background-color: transparent;-fx-border: none;");
         vue.getBizerte().setOnMouseEntered(e -> vue.getL().setGraphic(new ImageView( vue.getBizertei())));
         vue.getBizerte().setOnAction(e -> {
            vue.getCombo().setValue("Bizerte");c.getMarker().relocate(185, 6);
             vue.getWindow().close();
             c.getMarker().setVisible(true);
        });

         vue.getGabes().setStyle("-fx-background-color: transparent;-fx-border: none;");
        vue.getGabes().setOnMouseEntered(e -> vue.getL().setGraphic(new ImageView(vue.getGabesi())));
        vue.getGabes().setOnAction(e -> {
            vue.getCombo().setValue("Gabes");c.getMarker().relocate(182, 63);
             vue.getWindow().close();
             c.getMarker().setVisible(true);
        });

        vue.getGafsa().setStyle("-fx-background-color: transparent;-fx-border: none;");
        vue.getGafsa().setOnMouseEntered(e -> vue.getL().setGraphic(new ImageView(vue.getGafsai())));
        vue.getGafsa().setOnAction(e -> {
            vue.getCombo().setValue("Gafsa");c.getMarker().relocate(166, 55);
             vue.getWindow().close();
             c.getMarker().setVisible(true);
        });

        vue.getJendouba().setStyle("-fx-background-color: transparent;-fx-border: none;");
        vue.getJendouba().setOnMouseEntered(e -> vue.getL().setGraphic(new ImageView(vue.getJendoubai())));
        vue.getJendouba().setOnAction(e -> {
            vue.getCombo().setValue("Jendouba");
             vue.getWindow().close(); c.getMarker().relocate(174, 12);c.getMarker().setVisible(true);
        });

        vue.getKairouan().setStyle("-fx-background-color: transparent;-fx-border: none;");
        vue.getKairouan().setOnMouseEntered(e -> vue.getL().setGraphic(new ImageView(vue.getKairouani())));
       vue.getKairouan().setOnAction(e -> {
            vue.getCombo().setValue("Kairouan");c.getMarker().relocate(188, 30);
           c.getMarker().setVisible(true);
             vue.getWindow().close();
        });

        vue.getKasserine().setStyle("-fx-background-color: transparent;-fx-border: none;");
        vue.getKasserine().setOnMouseEntered(e -> vue.getL().setGraphic(new ImageView(vue.getKasserinei())));
        vue.getKasserine().setOnAction(e -> {
            vue.getCombo().setValue("Kesserine");c.getMarker().relocate(171, 40);
               vue.getWindow().close();
               c.getMarker().setVisible(true);
        });

        vue.getKebili1().setStyle("-fx-background-color: transparent;-fx-border: none;");
         vue.getKebili1().setOnMouseEntered(e -> vue.getL().setGraphic(new ImageView( vue.getKebilii())));
         vue.getKebili1().setOnAction(e -> {
             c.getMarker().setVisible(true);
            vue.getCombo().setValue("Kebili");c.getMarker().relocate(166, 77);
             vue.getWindow().close();
        });

         vue.getKebili2().setStyle("-fx-background-color: transparent;-fx-border: none;");
         vue.getKebili2().setOnMouseEntered(e -> vue.getL().setGraphic(new ImageView( vue.getKebilii())));
         vue.getKebili2().setOnAction(e -> {
           c.getMarker().setVisible(true);
            vue.getCombo().setValue("Kebili");c.getMarker().relocate(166, 77);
             vue.getWindow().close();
        });

         vue.getKef().setStyle("-fx-background-color: transparent;-fx-border: none;");
        vue.getKef().setOnMouseEntered(e -> vue.getL().setGraphic(new ImageView(vue.getKefi())));
        vue.getKef().setOnAction(e -> {
            c.getMarker().setVisible(true);
            vue.getCombo().setValue("Le Kef"); c.getMarker().relocate(171, 22);
             vue.getWindow().close();
        });

        vue.getMahdia().setStyle("-fx-background-color: transparent;-fx-border: none;");
        vue.getMahdia().setOnMouseEntered(e -> vue.getL().setGraphic(new ImageView(vue.getMahdiai())));
        vue.getMahdia().setOnAction(e -> {
            c.getMarker().setVisible(true);
            vue.getCombo().setValue("Mahdia");c.getMarker().relocate(205, 34);
             vue.getWindow().close();
        });

        vue.getManouba().setStyle("-fx-background-color: transparent;-fx-border: none;");
        vue.getManouba().setOnMouseEntered(e -> vue.getL().setGraphic(new ImageView(vue.getManoubai())));
        vue.getManouba().setOnAction(e -> {
            c.getMarker().setVisible(true);
            vue.getCombo().setValue("La Mannouba");c.getMarker().relocate(189, 13);
             vue.getWindow().close();
        });

        vue.getMedenine().setStyle("-fx-background-color: transparent;-fx-border: none;");
          vue.getMedenine().setOnMouseEntered(e -> vue.getL().setGraphic(new ImageView(  vue.getMedeninei())));
          vue.getMedenine().setOnAction(e -> {
              c.getMarker().setVisible(true);
            vue.getCombo().setValue("Médenine");c.getMarker().relocate(190, 75);
             vue.getWindow().close();
        });

          vue.getMedenine2().setStyle("-fx-background-color: transparent;-fx-border: none;");
          vue.getMedenine2().setOnMouseEntered(e -> vue.getL().setGraphic(new ImageView(  vue.getMedeninei())));
          vue.getMedenine2().setOnAction(e -> {
            c.getMarker().setVisible(true);
            vue.getCombo().setValue("Médenine");c.getMarker().relocate(190, 75);
             vue.getWindow().close();
        });

          vue.getMedenine3().setStyle("-fx-background-color: transparent;-fx-border: none;");
          vue.getMedenine3().setOnMouseEntered(e -> vue.getL().setGraphic(new ImageView(  vue.getMedeninei())));
          vue.getMedenine3().setOnAction(e -> {
              c.getMarker().setVisible(true);
            vue.getCombo().setValue("Médenine");c.getMarker().relocate(190, 75);
             vue.getWindow().close();
        });

          vue.getMonastir().setStyle("-fx-background-color: transparent;-fx-border: none;");
         vue.getMonastir().setOnMouseEntered(e -> vue.getL().setGraphic(new ImageView( vue.getMonastiri())));
         vue.getMonastir().setOnAction(e -> {
             c.getMarker().setVisible(true);
            vue.getCombo().setValue("Monastir");c.getMarker().relocate(202, 30);
             vue.getWindow().close();
        });

         vue.getNabeul().setStyle("-fx-background-color: transparent;-fx-border: none;");
         vue.getNabeul().setOnMouseEntered(e -> vue.getL().setGraphic(new ImageView(vue.getNabeuli())));
         vue.getNabeul().setOnAction(e -> {
            c.getMarker().setVisible(true);
            vue.getCombo().setValue("Nabeul"); vue.getCombo().setValue("Ben Arous");c.getMarker().relocate(202, 12);
             vue.getWindow().close();
        });

        vue.getSfax().setStyle("-fx-background-color: transparent;-fx-border: none;");
        vue.getSfax().setOnMouseEntered(e -> vue.getL().setGraphic(new ImageView(vue.getSfaxi())));
        vue.getSfax().setOnAction(e -> {
            c.getMarker().setVisible(true);
            vue.getCombo().setValue("Sfax");c.getMarker().relocate(197, 48);
             vue.getWindow().close();
        });

        vue.getSidibou().setStyle("-fx-background-color: transparent;-fx-border: none;");
        vue.getSidibou().setOnMouseEntered(e -> vue.getL().setGraphic(new ImageView(vue.getSidiboui())));
        vue.getSidibou().setOnAction(e -> {
          c.getMarker().setVisible(true);
            vue.getCombo().setValue("Sidi Bouzid");c.getMarker().relocate(182, 44);
             vue.getWindow().close();
        });

        vue.getSiliana().setStyle("-fx-background-color: transparent;-fx-border: none;");
         vue.getSiliana().setOnMouseEntered(e -> vue.getL().setGraphic(new ImageView( vue.getSilianai())));
         vue.getSiliana().setOnAction(e -> {
             c.getMarker().setVisible(true);
            vue.getCombo().setValue("Siliana");c.getMarker().relocate(183, 22);
             vue.getWindow().close();
        });

         vue.getSousse().setStyle("-fx-background-color: transparent;-fx-border: none;");
         vue.getSousse().setOnMouseEntered(e -> vue.getL().setGraphic(new ImageView( vue.getSoussei())));
         vue.getSousse().setOnAction(e -> {
             c.getMarker().setVisible(true);
            vue.getCombo().setValue("Sousse");c.getMarker().relocate(198, 25);
            
             vue.getWindow().close();
        });

         vue.getTataouine().setStyle("-fx-background-color: transparent;-fx-border: none;");
         vue.getTataouine().setOnMouseEntered(e -> vue.getL().setGraphic(new ImageView( vue.getTataouinei())));
         vue.getTataouine().setOnAction(e -> {
            c.getMarker().setVisible(true);
            vue.getCombo().setValue("Tataouine");c.getMarker().relocate(188, 100);
             vue.getWindow().close();
        });

        vue.getTozeur().setStyle("-fx-background-color: transparent;-fx-border: none;");
        vue.getTozeur().setOnMouseEntered(e -> vue.getL().setGraphic(new ImageView(vue.getTozeuri())));
        vue.getTozeur().setOnAction(e -> {
           c.getMarker().setVisible(true);
            vue.getCombo().setValue("Tozeur");c.getMarker().relocate(154, 65);
             vue.getWindow().close();
        });

        vue.getTunis().setStyle("-fx-background-color: transparent;-fx-border: none;");
         vue.getTunis().setOnMouseEntered(e -> vue.getL().setGraphic(new ImageView( vue.getTunisi())));
         vue.getTunis().setOnAction(e -> {
            c.getMarker().setVisible(true);
            vue.getCombo().setValue("Tunis");c.getMarker().relocate(195, 12);
             vue.getWindow().close();
        });

         vue.getZaghouan().setStyle("-fx-background-color: transparent;-fx-border: none;");
         vue.getZaghouan().setOnMouseEntered(e -> vue.getL().setGraphic(new ImageView( vue.getZaghouani())));
         vue.getZaghouan().setOnAction(e -> {
            c.getMarker().setVisible(true);
           vue.getCombo().setValue("Zaghouan");c.getMarker().relocate(191, 18);
             vue.getWindow().close();
        });
         
          vue.getWindow().focusedProperty().addListener((e, v1, v2) -> {
            if (v2 == false) {
               

                vue.getWindow().close();
                vue.getAccueil().getStage().getScene().getRoot().setEffect(null);
            }
        });

          
        vue.getBp().setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
              
                 x = vue.getWindow().getX() - mouseEvent.getScreenX();
                 y = vue.getWindow().getY() - mouseEvent.getScreenY();
            }
        });
        vue.getBp().setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                vue.getWindow().setX(mouseEvent.getScreenX() + x);
                vue.getWindow().setY(mouseEvent.getScreenY() + y);
            }
        });
        
    }
}
