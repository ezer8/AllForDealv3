/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.esprit.pidev.vues;

import edu.esprit.pidev.dao.classes.EvaluationDao;
import edu.esprit.pidev.dao.classes.MembreDao;
import edu.esprit.pidev.entities.Evaluation;
import edu.esprit.pidev.entities.Membre;
import edu.esprit.pidev.entities.Produit;
import edu.esprit.pidev.entities.Service;
import java.io.File;
import java.net.URL;
import java.sql.Date;
import java.util.List;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.Border;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import org.controlsfx.control.Rating;
import static sun.plugin.javascript.navig.JSType.Image;

/**
 * FXML Controller class
 *
 * @author Asus
 */
public class ProduitController implements Initializable {
    @FXML
    TextArea textarea;
    @FXML
    ImageView im; 
    @FXML Label titre,utilisateur,categorie,date,note,prix,counts;
    @FXML Rating ratingz;
    TextArea text;
    Pane ajoutCommentaire;
    EvaluationDao evaluationDao;
   
    public static Produit produit;
    List <Evaluation> listEva;
    Double moyenne= 0d;
    long count = 0;
    /**
     * Initializes the controller class.
     */
    @FXML private Rating rating;
    @FXML private VBox vbox;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        evaluationDao = new EvaluationDao();
        ImageView iv = new ImageView();
        iv.setFitHeight(16);
        iv.setFitWidth(16);
        ratingz.setPartialRating(true);
        ratingz.setUpdateOnHover(false);
        listEva= evaluationDao.selectByProduit(produit);
        try{
        moyenne = listEva.stream().mapToInt(ev->ev.getEvaluation()).filter(d->d>0).average().getAsDouble();}
        catch (Exception e){}
        count =listEva.stream().mapToInt(ev->ev.getEvaluation()).filter(d->d>0).count();
        counts.setText(count+"");
         
        note.setText("Note : "+Math.round( moyenne * 10.0 ) / 10.0+"/5.0" );
        ratingz.setRating(Math.round( moyenne * 10.0 ) / 10.0);
        titre.setText(produit.getLibelle());
        categorie.setText(produit.getCategorie().getLibelle());
      
        date.setText(produit.getDate()+"");
        prix.setText(produit.getPrix()+" Dt");
        im.setImage(new Image(new File(produit.getImage()).toURI().toString()));
        iv.setImage(new Image(new File("C:\\Users\\Asus\\Documents\\NetBeansProjects\\AllForDealv2\\src\\edu\\esprit\\pidev\\images\\users-icon.png").toURI().toString()));
        counts.setGraphic(iv);
        textarea.setEditable(false);
        textarea.setText(produit.getDescription());
        textarea.setWrapText(true);
        textarea.setBackground(Background.EMPTY);
        
        textarea.setBorder(Border.EMPTY);
        rating.setRating(0);
               
        for (Evaluation eva : listEva)
        { 
            createCommentaire(eva);}
        
          commentaire();
        
            
            
            
    }    
   
  
            
    
      public void  createCommentaire(Evaluation eva) {
            
        Pane p = new Pane();
        Label titre = new Label(eva.getMembre().getLogin());
          ImageView image = new ImageView();
           HBox  r= new HBox();
     
        image.setImage(new Image(new File("C:\\Users\\Asus\\Documents\\NetBeansProjects\\AllForDealv2\\src\\edu\\esprit\\pidev\\images\\profil.png").toURI().toString()));
        Label date = new Label(eva.getDate()+"");
        Text txt = new Text();
        txt.setText(eva.getCommentaire());
        txt.setWrappingWidth(330);
     
        image.setFitWidth(40);
        image.setFitHeight(40);
        
        titre.relocate(10, 4);
        image.relocate(10, 36);
        txt.relocate(105, 10);
        date.relocate(430,70);
        r.setPrefSize(300, 20);
        r.relocate(105, 60);
        for (int i =0;i<eva.getEvaluation();i++){
           ImageView stars[] = new ImageView[eva.getEvaluation()];
          stars[i] = new ImageView();
             stars[i].setImage(new Image(new File("C:\\Users\\Asus\\Documents\\NetBeansProjects\\AllForDealv2\\src\\edu\\esprit\\pidev\\images\\etoile.gif").toURI().toString()));
        r.getChildren().add(stars[i]);
        }
           p.getChildren().addAll(titre,txt,date,image,r);
     
        p.setPrefSize(498, 90);
        
        p.setStyle("  -fx-background-color: \n"
                + "        linear-gradient(#FFFFFF, #d6d6d6),\n"
                + "        linear-gradient(#FFFFFF 0%, #E1E2E1 20%, #d6d6d6 100%),\n"
                + "        linear-gradient(#FFFFFF 0%, #f6f6f6 50%);\n"
          
                + "    -fx-background-insets: 0,1,2;\n"
                + "    -fx-text-fill: black;\n"
              );
      
       vbox.getChildren().add(p);
        
    }
       public void  createCustomCommentaire() {
            
        Pane p = new Pane();
        Label titre = new Label("Nom");
          ImageView image = new ImageView();
        Double dou = rating.getRating();
        HBox  h= new HBox();
        image.setImage(new Image(new File("C:\\Users\\Asus\\Documents\\NetBeansProjects\\AllForDealv2\\src\\edu\\esprit\\pidev\\images\\profil.png").toURI().toString()));
        Date d= new Date(new java.util.Date().getTime());
        Label date = new Label(d+"");
        Text txt = new Text();
        txt.setText(text.getText());
        text.setText("");
        txt.setWrappingWidth(330);
        p.getChildren().addAll(titre,txt,date,image,h);
        image.setFitWidth(40);
        image.setFitHeight(40);
        
        titre.relocate(10, 4);
        image.relocate(10, 36);
        txt.relocate(105, 10);
        date.relocate(430,70);
       
        
             h.setPrefSize(300, 20);
        h.relocate(105, 60);
        for (int i =0;i<dou.intValue();i++){
           ImageView stars[] = new ImageView[dou.intValue()];
          stars[i] = new ImageView();
             stars[i].setImage(new Image(new File("C:\\Users\\Asus\\Documents\\NetBeansProjects\\AllForDealv2\\src\\edu\\esprit\\pidev\\images\\etoile.gif").toURI().toString()));
        h.getChildren().add(stars[i]);
        }
        p.setPrefSize(498, 90);
        
        p.setStyle("  -fx-background-color: \n"
                + "        linear-gradient(#FFFFFF, #d6d6d6),\n"
                + "        linear-gradient(#FFFFFF 0%, #E1E2E1 20%, #d6d6d6 100%),\n"
                + "        linear-gradient(#FFFFFF 0%, #f6f6f6 50%);\n"
          
                + "    -fx-background-insets: 0,1,2;\n"
                + "    -fx-text-fill: black;\n"
              );
      
       vbox.getChildren().add(p);
        
    }
       public void commentaire() {
            
         ajoutCommentaire = new Pane();
        Label titre = new Label("Nom");
        ImageView image = new ImageView();
        image.setImage(new Image(new File("C:\\Users\\Asus\\Documents\\NetBeansProjects\\AllForDealv2\\src\\edu\\esprit\\pidev\\images\\profil.png").toURI().toString()));
        Button button = new Button("Envoyer");
   
        text = new TextArea();
     
        ajoutCommentaire.getChildren().addAll(titre,text,button,image);
        image.setFitHeight(40);
        image.setFitWidth(40);
        text.setPrefSize(298, 60);
      
        titre.relocate(10, 4);
        image.relocate(10, 36);
        text.relocate(110, 10);
        button.relocate(420, 29);
        text.setWrapText(true);
        button.setOnAction(e->{if (!text.getText().equals(""))
        {vbox.getChildren().remove(ajoutCommentaire);
     
        Date d= new Date(new java.util.Date().getTime());
        Double rate= rating.getRating();
        evaluationDao.add(new Evaluation(rate.intValue(),text.getText(),d,produit,new Service(),new Membre(1)));
        createCustomCommentaire();
        rating.setRating(0);
        vbox.getChildren().add(ajoutCommentaire);
         listEva= evaluationDao.selectByProduit(produit);
        moyenne = listEva.stream().mapToInt(ev->ev.getEvaluation()).filter(x->x>0).average().getAsDouble();
         count =listEva.stream().mapToInt(ev->ev.getEvaluation()).filter(x->x>0).count();
        counts.setText(count+ "");
        note.setText("Note : "+Math.round( moyenne * 10.0 ) / 10.0+"/5.0");
        ratingz.setRating(Math.round( moyenne * 10.0 ) / 10.0);
     
      
        }});
            text.setOnKeyPressed(e->{
            
              if (text.getText().length() > 80) {
                String s = text.getText().substring(0, 80);
                text.setText(s);
                text.positionCaret(80);
            }
            });
        ajoutCommentaire.setPrefSize(498, 90);
        
        ajoutCommentaire.setStyle("  -fx-background-color: \n"
                + "        linear-gradient(#FFFFFF, #d6d6d6),\n"
                + "        linear-gradient(#FFFFFF 0%, #d9d9d9 20%, #d6d6d6 100%),\n"
                + "        linear-gradient(#FFFFFF 0%, #f6f6f6 50%);\n"
             
                + "    -fx-background-insets: 0,1,2;\n"
                + "    -fx-text-fill: black;\n"
              );
      
       vbox.getChildren().add(ajoutCommentaire);
        
    }
    
}
