/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.esprit.pidev.vues;

import edu.esprit.pidev.entities.Membre;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;

/**
 * FXML Controller class
 *
 * @author Shamileth
 */
public class ModifProfController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    TextField nbprod, nbserv, reviewtotal, nom, prenom, age;
    Membre m;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
     
        nom.setText( "");
        prenom.setText( "");
        age.setText( "");
        nbprod.setText("");
        nbserv.setText("");
        reviewtotal.setText("");
    }
}
