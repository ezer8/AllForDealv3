/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.esprit.pidev.vues;

import edu.esprit.pidev.controllers.PaysControl;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.effect.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author Asus
 */
public class Pays {

    private String rep = "";
    double x, y;
    ComboBox combo;
    Stage window;
    Accueil accueil;
    Label l;
    BorderPane bp;
    private Button ariana, beja1, beja2, benArous, bizerte, gabes, gafsa, jendouba, kairouan, kasserine, kebili1, kebili2, kef, mahdia, manouba, medenine, medenine2, medenine3, monastir, nabeul, sfax, sidibou, siliana, sousse, tataouine, tozeur, tunis, zaghouan;
    private Image arianai, bejai, benArousi, bizertei, gabesi, gafsai, jendoubai, kairouani, kasserinei, kebilii, kefi, mahdiai, manoubai, medeninei,  monastiri, nabeuli, sfaxi, sidiboui, silianai, soussei, tataouinei, tozeuri, tunisi, zaghouani;

    public Pays(Accueil accueil,ComboBox combo) {
         ariana = new Button();
        beja1 = new Button();
        beja2 = new Button();
        benArous = new Button();
        bizerte = new Button();
        gabes = new Button();
        gafsa = new Button();
        jendouba = new Button();
        kairouan = new Button();
        kasserine = new Button();
        kebili1 = new Button();
        kebili2 = new Button();
        kef = new Button();
        mahdia = new Button();
        manouba = new Button();
        medenine = new Button();
        medenine2 = new Button();
        medenine3 = new Button();
        monastir = new Button();
        nabeul = new Button();
        sfax = new Button();
        sidibou = new Button();
        siliana = new Button();
        sousse = new Button();
        tataouine = new Button();
        tozeur = new Button();
        tunis = new Button();
        zaghouan = new Button();
        this.combo = combo;
        this.accueil= accueil;
        window = new Stage();
    }
    
    public void display(Accueil accueil,ComboBox combo) {
        Accueil.a.getStage().getScene().getRoot().setEffect(new GaussianBlur());
        
       
    ariana = new Button();
        beja1 = new Button();
        beja2 = new Button();
        benArous = new Button();
        bizerte = new Button();
        gabes = new Button();
        gafsa = new Button();
        jendouba = new Button();
        kairouan = new Button();
        kasserine = new Button();
        kebili1 = new Button();
        kebili2 = new Button();
        kef = new Button();
        mahdia = new Button();
        manouba = new Button();
        medenine = new Button();
        medenine2 = new Button();
        medenine3 = new Button();
        monastir = new Button();
        nabeul = new Button();
        sfax = new Button();
        sidibou = new Button();
        siliana = new Button();
        sousse = new Button();
        tataouine = new Button();
        tozeur = new Button();
        tunis = new Button();
        zaghouan = new Button();
        this.combo = combo;
        this.accueil= accueil;
        window = new Stage();
        //window.initModality(Modality.APPLICATION_MODAL);
       
        window.setTitle("Tunisie");
        bp = new BorderPane();
        Pane right = new Pane();
        Pane left = new Pane();
        Pane bot = new Pane();
        Pane top = new Pane();
        top.setPrefSize(344, 2);
        right.setPrefSize(2, 630);
        left.setPrefSize(2, 630);
        bot.setPrefSize(344, 2);
        Pane p = new Pane();
        bp.setTop(top);
        bp.setCenter(p);
        bp.setLeft(left);
        bp.setBottom(bot);
        bp.setRight(right);

       l = new Label();
        Image image = new Image("edu/esprit/pidev/images/tunivide.png");
        arianai = new Image("edu/esprit/pidev/images/states/ariana.png");
        bejai = new Image("edu/esprit/pidev/images/states/beja.png");
        benArousi = new Image("edu/esprit/pidev/images/states/ben arouss.png");
        bizertei = new Image("edu/esprit/pidev/images/states/bizerte.png");
        gabesi = new Image("edu/esprit/pidev/images/states/gabes.png");
        gafsai = new Image("edu/esprit/pidev/images/states/gafsa.png");
        jendoubai = new Image("edu/esprit/pidev/images/states/jendouba.png");
        kairouani = new Image("edu/esprit/pidev/images/states/kairaouan.png");
        kasserinei = new Image("edu/esprit/pidev/images/states/kasserine.png");
        kebilii = new Image("edu/esprit/pidev/images/states/kebeli.png");
        kefi = new Image("edu/esprit/pidev/images/states/le kef.png");
        mahdiai = new Image("edu/esprit/pidev/images/states/mahdia.png");
        manoubai = new Image("edu/esprit/pidev/images/states/manouba.png");
        medeninei = new Image("edu/esprit/pidev/images/states/mednine.png");
        monastiri = new Image("edu/esprit/pidev/images/states/mounastir.png");
        nabeuli = new Image("edu/esprit/pidev/images/states/nabeul.png");
        sfaxi = new Image("edu/esprit/pidev/images/states/sfax.png");
        sidiboui = new Image("edu/esprit/pidev/images/states/sidi bouzid.png");
        silianai = new Image("edu/esprit/pidev/images/states/siliana.png");
        soussei = new Image("edu/esprit/pidev/images/states/sousse.png");
        tataouinei = new Image("edu/esprit/pidev/images/states/tataouine.png");
        tozeuri = new Image("edu/esprit/pidev/images/states/tozeur.png");
        tunisi = new Image("edu/esprit/pidev/images/states/tunis.png");
        zaghouani = new Image("edu/esprit/pidev/images/states/zaghouane.png");
        l.setGraphic(new ImageView(image));
        p.getChildren().addAll(l, ariana, beja1, beja2, benArous, bizerte, gabes, gafsa, jendouba, kairouan, kasserine, kebili1, kebili2, kef, mahdia, manouba, medenine, medenine2, medenine3, monastir, nabeul, sfax, sidibou, siliana, sousse, tataouine, tozeur, tunis, zaghouan);

        Scene scene = new Scene(bp, 344, 634);
        bp.setStyle("-fx-background-color: grey;");
        window.initStyle(StageStyle.TRANSPARENT);
        // p.setStyle("-fx-border-color: black, transparent; -fx-border-width: 1px, 5px; -fx-border-insets: 0px, 1px"); 
        window.setScene(scene);
        window.show();
        ariana.setPrefSize(20, 18);
        ariana.relocate(205, 47);
        beja1.setPrefSize(22, 22);
        beja1.relocate(127, 52);
        beja2.setPrefSize(50, 28);
        beja2.relocate(136, 79);
        benArous.setPrefSize(18, 18);
        benArous.relocate(211, 81);
        bizerte.setPrefSize(65, 35);
        bizerte.relocate(142, 30);
        gabes.setPrefSize(67, 67);
        gabes.relocate(160, 293);
        gafsa.setPrefSize(88, 45);
        gafsa.relocate(70, 252);
        jendouba.setPrefSize(45, 45);
        jendouba.relocate(85, 66);
        kairouan.setPrefSize(49, 65);
        kairouan.relocate(169, 142);
        kasserine.setPrefSize(62, 72);
        kasserine.relocate(87, 170);
        kebili1.setPrefSize(115, 72);
        kebili1.relocate(62, 344);
        kebili2.setPrefSize(60, 40);
        kebili2.relocate(95, 304);
        kef.setPrefSize(55, 57);
        kef.relocate(82, 110);
        mahdia.setPrefSize(65, 22);
        mahdia.relocate(222, 184);
        manouba.setPrefSize(27, 22);
        manouba.relocate(177, 66);
        medenine.setPrefSize(80, 22);
        medenine.relocate(218, 347);
        medenine2.setPrefSize(50, 60);
        medenine2.relocate(270, 369);
        medenine3.setPrefSize(20, 20);
        medenine3.relocate(200, 368);
        monastir.setPrefSize(20, 20);
        monastir.relocate(251, 162);
        nabeul.setPrefSize(50, 50);
        nabeul.relocate(235, 56);
        sfax.setPrefSize(70, 70);
        sfax.relocate(198, 215);
        sidibou.setPrefSize(50, 65);
        sidibou.relocate(145, 202);
        siliana.setPrefSize(33, 65);
        siliana.relocate(139, 111);
        sousse.setPrefSize(33, 60);
        sousse.relocate(221, 110);
        tataouine.setPrefSize(170, 210);
        tataouine.relocate(124, 404);
        tozeur.setPrefSize(55, 65);
        tozeur.relocate(26, 277);
        tunis.setPrefSize(1, 1);
        tunis.relocate(209, 62);
        zaghouan.setPrefSize(35, 40);
        zaghouan.relocate(184, 95);
        
        new PaysControl(this).control();

        if (combo.getSelectionModel().getSelectedIndex() != -1) {
            switch (combo.getSelectionModel().getSelectedItem().toString()) {

                case "Ariana":
                    l.setGraphic(new ImageView(arianai));
                    
                    break;
                case "Beja":
                     l.setGraphic(new ImageView(bejai));
                    break;
                case "Ben Arous":
                     l.setGraphic(new ImageView(benArousi));
                    break;
                case "Bizerte":
                     l.setGraphic(new ImageView(bizertei));
                    break;
                case "Gabes":
                     l.setGraphic(new ImageView(gabesi));
                    break;
                case "Gafsa":
                     l.setGraphic(new ImageView(gafsai));
                    break;
                case "Jendouba":
                     l.setGraphic(new ImageView(jendoubai));
                    break;
                case "Kairouan":
                     l.setGraphic(new ImageView(kairouani));
                    break;
                case "Kasserine":
                     l.setGraphic(new ImageView(kasserinei));
                    break;
                case "Kebili":
                     l.setGraphic(new ImageView(kebilii));
                    break;
                case "Le Kef":
                     l.setGraphic(new ImageView(kefi));
                    break;
                case "Mahdia":
                     l.setGraphic(new ImageView(mahdiai));
                    break;
                case "La Manouba":
                     l.setGraphic(new ImageView(manoubai));
                    break;
                case "Médenine":
                     l.setGraphic(new ImageView(medeninei));
                    break;
                case "Monastir":
                   l.setGraphic(new ImageView(monastiri));
                    break;
                case "Nabeul":
                    l.setGraphic(new ImageView(nabeuli));
                    break;
                case "Sfax":
                    l.setGraphic(new ImageView(sfaxi));
                    break;
                case "Sidi Bouzid":
                   l.setGraphic(new ImageView(sidiboui));
                    break;
                case "Siliana":
                     l.setGraphic(new ImageView(silianai));
                    break;
                case "Sousse":
                     l.setGraphic(new ImageView(soussei));
                    break;
                case "Tataouine":
                  l.setGraphic(new ImageView(tataouinei));
                    break;
                case "Tozeur":
              l.setGraphic(new ImageView(tozeuri));
                    break;
                case "Tunis":
                    l.setGraphic(new ImageView(tunisi));
                    break;
                case "Zaghouan":
                  l.setGraphic(new ImageView(zaghouani));
                    break;
         
            }
        }


       
    }

    public String getRep() {
        return rep;
    }

 

    public Button getAriana() {
        return ariana;
    }

    public Button getBeja1() {
        return beja1;
    }

    public Button getBeja2() {
        return beja2;
    }

    public Button getBenArous() {
        return benArous;
    }

    public Button getBizerte() {
        return bizerte;
    }

    public Button getGabes() {
        return gabes;
    }

    public Button getGafsa() {
        return gafsa;
    }

    public Button getJendouba() {
        return jendouba;
    }

    public Button getKairouan() {
        return kairouan;
    }

    public Button getKasserine() {
        return kasserine;
    }

    public Button getKebili1() {
        return kebili1;
    }

    public Button getKebili2() {
        return kebili2;
    }

    public Button getKef() {
        return kef;
    }

    public Button getMahdia() {
        return mahdia;
    }

    public Button getManouba() {
        return manouba;
    }

    public Button getMedenine() {
        return medenine;
    }

    public Button getMedenine2() {
        return medenine2;
    }

    public Button getMedenine3() {
        return medenine3;
    }

    public Button getMonastir() {
        return monastir;
    }

    public Button getNabeul() {
        return nabeul;
    }

    public Button getSfax() {
        return sfax;
    }

    public Button getSidibou() {
        return sidibou;
    }

    public Button getSiliana() {
        return siliana;
    }

    public Button getSousse() {
        return sousse;
    }

    public Button getTataouine() {
        return tataouine;
    }

    public Button getTozeur() {
        return tozeur;
    }

    public Button getTunis() {
        return tunis;
    }

    public Button getZaghouan() {
        return zaghouan;
    }

    public Image getArianai() {
        return arianai;
    }

    public Image getBejai() {
        return bejai;
    }

    public Image getBenArousi() {
        return benArousi;
    }

    public Image getBizertei() {
        return bizertei;
    }

    public Image getGabesi() {
        return gabesi;
    }

    public Image getGafsai() {
        return gafsai;
    }

    public Image getJendoubai() {
        return jendoubai;
    }

    public Image getKairouani() {
        return kairouani;
    }

    public Image getKasserinei() {
        return kasserinei;
    }

    public Image getKebilii() {
        return kebilii;
    }

    public Image getKefi() {
        return kefi;
    }

    public Image getMahdiai() {
        return mahdiai;
    }

    public Image getManoubai() {
        return manoubai;
    }

    public Image getMedeninei() {
        return medeninei;
    }

    public Image getMonastiri() {
        return monastiri;
    }

    public Image getNabeuli() {
        return nabeuli;
    }

    public Image getSfaxi() {
        return sfaxi;
    }

    public Image getSidiboui() {
        return sidiboui;
    }

    public Image getSilianai() {
        return silianai;
    }

    public Image getSoussei() {
        return soussei;
    }

    public Image getTataouinei() {
        return tataouinei;
    }

    public Image getTozeuri() {
        return tozeuri;
    }

    public Image getTunisi() {
        return tunisi;
    }

    public Image getZaghouani() {
        return zaghouani;
    }

    public ComboBox getCombo() {
        return combo;
    }

    public Stage getWindow() {
        return window;
    }

    public Label getL() {
        return l;
    }

    public BorderPane getBp() {
        return bp;
    }

    public Accueil getAccueil() {
        return accueil;
    }

   

    
}
