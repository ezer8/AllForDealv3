/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.esprit.pidev.vues;

import edu.esprit.pidev.entities.Membre;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;

/**
 * FXML Controller class
 *
 * @author Shamileth
 */
public class ProfilDcController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    Label nbprod, nbserv, reviewtotal, prodvendu, nomprenom, message, i;
    Membre m;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        m = new Membre("Ghassen", "Nasri", 22, "nasrighassen@gmail.com", "21711442", "5qsdaze azeaz", new java.sql.Date(new java.util.Date().getTime()), "particulier");
        nomprenom.setText(m.getNom() + " " + m.getPrenom() + " ( " + m.getAge() + " )");
        message.setText("Derniere Visite : " + m.getDateVisite() + "");
        nbprod.setText("E-Mail : " + m.getEmail() + "");
        nbserv.setText("Adresse : " + m.getAdresse());
        reviewtotal.setText("Tel : " + m.getNumeroTel());

    }
}
