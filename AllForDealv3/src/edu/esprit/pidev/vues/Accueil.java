/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.esprit.pidev.vues;

import displayshelf.Shelf;
import edu.esprit.pidev.entities.Membre;
import edu.esprit.pidev.entities.Panier;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


/**
 *
 * @author Asus
 */
public class Accueil extends Application {
    public static Accueil a;
   public static Stage stage;
   public static Panier panier;
    Parent root;
    VBox vbox;
    
    public static Scene scene;
    @Override
    public void start(Stage stage) throws Exception {
        this.stage = stage;
         panier= new Panier(new Membre(1));
        
        a= this;
       Parent root = FXMLLoader.load(getClass().getResource("Accueil0.fxml"));
         //Parent root = FXMLLoader.load(getClass().getResource("produit.fxml"));
        BorderPane root2 = FXMLLoader.load(getClass().getResource("Accueil.fxml"));
       Parent root3 = FXMLLoader.load(getClass().getResource("Authentification.fxml"));
        
        
        Image[] images = new Image[14];
         for (int i = 0; i < 14; i++) {
            images[i] = new Image( Shelf.class.getResource("/displayshelf/animal"+(i+1)+".jpg").toExternalForm(),false);
        }
        
        VBox vbox = new VBox();
        // create display shelf
        Shelf displayShelf = new Shelf(images);
        displayShelf.setPrefSize(300, 500);
        //
        vbox.getChildren().addAll(root,displayShelf);
        root2.setCenter(vbox);
        root2.setLeft(null);
        
        this.root = root;
      
    
        scene = new Scene(root3);
        scene.getStylesheets().add(getClass().getResource("/edu/esprit/pidev/technique/css/Accueil.css").toExternalForm());
        this.stage.setScene(scene);
        this.stage.show();
        System.out.println(Accueil.stage);
    }

    public Stage getStage() {
        return stage;
    }
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
 
    public void startapp(){
        launch();
    }
}
