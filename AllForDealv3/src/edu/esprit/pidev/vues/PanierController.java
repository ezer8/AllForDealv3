/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.esprit.pidev.vues;

import edu.esprit.pidev.dao.classes.PanierDao;
import edu.esprit.pidev.entities.Panier;
import edu.esprit.pidev.entities.Produit;
import static edu.esprit.pidev.vues.ProduitController.produit;
import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Shamileth
 */
public class PanierController implements Initializable {

 
    Panier p;

    Produit p1, p2, p3, p4, p5, p6;
    @FXML
    Label total;
    @FXML
    VBox vbox;
    
    PanierDao panierdao;

    @Override

    public void initialize(URL url, ResourceBundle rb) {
        panierdao = new PanierDao();
        p = panierdao.selectById(1);

        p1 = new Produit(1, "Telephone", 300, "/tel.jpg");
        p2 = new Produit(2, "Ordinateur", 1200, "/pc.jpg");
        p3 = new Produit(3, "Camera", 500, "/cam.jpg");
        p4 = new Produit(4, "Tablette", 450, "/tab.jpg");
        p5 = new Produit(5, "WebCam", 600, "/cam.jpg");
       
        initPanier();
        p.calculTotatP();
        total.setText(p.getTotal()+"");
    }

    @FXML
    public void ViderPressed(ActionEvent e) {
        vbox.getChildren().clear();
        panierdao.delete(p);
        p.viderPanier();
        
        total.setText("0.0");
    }
    
    



    public void fillPanier(Produit x) {
        Pane pan = new Pane();
        ImageView  img = new ImageView();
        img.setImage(new Image(new File(x.getImage()).toURI().toString()));
        Label nomP = new Label(x.getLibelle());
        Label qte = new Label("Qte.");
        TextField qtep = new TextField(p.getProduitsPanier().get(x)+"");
       
        
        Label prixtot = new Label((p.getProduitsPanier().get(x)*x.getPrix())+"");
         qtep.focusedProperty().addListener((e,v1,v2)->{
             
        if (v2 == false){
               if (Integer.parseInt(qtep.getText()) == 0)
            qtep.setText("1");
        prixtot.setText((Integer.parseInt(qtep.getText())*x.getPrix())+"");
        p.modifQuantiteP(x,Integer.parseInt(qtep.getText()) );
        p.calculTotatP();
        total.setText(p.getTotal()+"");
        panierdao.update(p);
        }
        
        });
        Button retirer = new Button("retirer");

        retirer.setOnAction(e -> {vbox.getChildren().remove(retirer.getParent());
        p.deleteProduitP(x);
        panierdao.delete(p);
        panierdao.add(p);
    
        p.calculTotatP();
        total.setText(p.getTotal()+"");
        
        });
        pan.setPrefSize(801, 135);

        img.setFitHeight(117);
        img.setFitWidth(117);
        img.relocate(14, 9);

        nomP.setPrefSize(199, 50);
        nomP.relocate(145, 43);

        qte.setPrefSize(44, 44);
        qte.relocate(401, 47);
        qte.setStyle("  -fx-font-size: 21");
        qtep.setPrefSize(44, 44);
        qtep.relocate(455, 47);

        retirer.setPrefSize(115, 30);
        retirer.relocate(668, 14);

        prixtot.setPrefSize(139, 50);
        prixtot.relocate(644, 76);
        prixtot.setStyle("  -fx-font-size: 28;-fx-font-weight: bold; -fx-text-align: right;");
        pan.getChildren().addAll(img, nomP, qte, qtep, retirer, prixtot);
        vbox.getChildren().add(pan);
    }

    public void initPanier() {
        for (Produit x : p.getProduitsP()) {
            fillPanier(x);
        }
    }
}
