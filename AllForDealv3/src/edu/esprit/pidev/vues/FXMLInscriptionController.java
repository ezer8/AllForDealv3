/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.esprit.pidev.vues;

import edu.esprit.pidev.dao.classes.MembreDao;
import edu.esprit.pidev.entities.Membre;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Armin
 */
public class FXMLInscriptionController implements Initializable {

    @FXML
    private TextField login, nom, prenom, age, email, numerotel, addresse, img;
    @FXML
    private PasswordField password;
    @FXML
    private ComboBox gender, type;

    @FXML
    private void SubmitAction(ActionEvent e) {
        MembreDao mdao = new MembreDao();
        if (!login.getText().equals("") && !nom.getText() .equals("") &&! prenom.getText() .equals("") && !age.getText() .equals("")
                && !email.getText() .equals("") && !numerotel.getText() .equals("") && !addresse.getText() .equals("") && !password.getText() .equals("")) {
           Date date = new Date();
            int a = Integer.valueOf(age.getText());
            Membre m = new Membre(login.getText(), password.getText(), nom.getText(), prenom.getText(), a,
                 email.getText(), numerotel.getText(), addresse.getText(),0,date , (String)type.getSelectionModel().getSelectedItem());
            if (!AuthentificationController.connection.equals("FB"))
                mdao.add(m);
            else {
                int id = Integer.valueOf(JavaApplication.id.substring(10));
                m.setId(id);
                mdao.add2(m);
            }
            AuthentificationController.c.browserStage.close();
        }else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("You need to fill the fields");
            alert.showAndWait();
        }
    }

    @FXML
    private void resetAction(ActionEvent e) {

    }
    @FXML
    private void importAction(ActionEvent e) throws IOException{
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.getExtensionFilters().addAll(new ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"));
        Stage stage = new Stage();
        File file = fileChooser.showOpenDialog(stage);
        if ( file!= null){
            img.setText(file.getAbsolutePath());
        }
    }
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        gender.getItems().addAll("male", "female");
        type.getItems().addAll("particulier", "entreprise");
        type.setValue("particulier");
        if (!AuthentificationController.connection.equals("FB")){
        gender.setValue("male");
        }else {
            nom.setText(JavaApplication.lname);
            prenom.setText(JavaApplication.fname);
            age.setText(JavaApplication.age);
            email.setText(JavaApplication.email);
            addresse.setText(JavaApplication.location);
            gender.setValue(JavaApplication.gender);
            img.setText(JavaApplication.img);
        }
        // TODO
    }

}
