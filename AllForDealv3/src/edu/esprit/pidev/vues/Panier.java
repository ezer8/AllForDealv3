/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.esprit.pidev.vues;

import java.io.IOException;
import javafx.animation.FadeTransition;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import javafx.scene.shape.*;
import javafx.animation.*;
import static javafx.application.Application.launch;
import javafx.scene.control.Button;
import javafx.scene.transform.Rotate;
import javafx.stage.Modality;

/**
 *
 * @author Shamileth
 */
public class Panier extends Application {

    @FXML
    Label c1;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        Parent root = FXMLLoader.load(getClass().getResource("panier.fxml"));
//        root.setStyle("-fx-background-color: transparent;");
        Scene scene = new Scene(root, Color.TRANSPARENT);
        scene.getStylesheets().add(getClass().getResource("style_panier.css").toExternalForm());
        primaryStage.initModality(Modality.APPLICATION_MODAL);

        primaryStage.setScene(scene);
        primaryStage.show();
    }

}
