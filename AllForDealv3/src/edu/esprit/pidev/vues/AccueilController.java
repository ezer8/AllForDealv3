/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.esprit.pidev.vues;

import displayshelf.Shelf;
import edu.esprit.pidev.dao.classes.CategorieProduitDao;
import edu.esprit.pidev.dao.classes.CategorieServiceDao;
import edu.esprit.pidev.dao.classes.PanierDao;
import edu.esprit.pidev.dao.classes.ProduitDao;
import edu.esprit.pidev.dao.classes.ServiceDao;
import edu.esprit.pidev.entities.CategorieProduit;
import edu.esprit.pidev.entities.CategorieService;
import edu.esprit.pidev.entities.Produit;
import edu.esprit.pidev.entities.Service;
import edu.esprit.pidev.entities.Membre;
import static edu.esprit.pidev.vues.Accueil.scene;
import static edu.esprit.pidev.vues.ProduitController.produit;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.effect.Glow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafxapplication3.JavaFXApplication3;
import org.controlsfx.control.RangeSlider;

/**
 *
 * @author Asus
 */
public class AccueilController implements Initializable {

    public static AccueilController c;
    ProduitDao produitDao;
    ServiceDao serviceDao;
    CategorieServiceDao categorieServiceDao;
    CategorieProduitDao categorieProduitDao;
    List<Produit> produits;
    List<Service> services;
    Pays vue;
    

    List<Produit> streamp;
    List<Service> streams;
     List<Produit> streamp1;
    List<Service> streams1;

    @FXML 
    private ToggleButton tous,particulier,entreprise;
    @FXML
    private TextField maxt,mint;
    @FXML
    private Pane recherchePays, recherchePrix;
    @FXML
    private VBox vbox;
    @FXML
    private Label marker;
    @FXML
    private Button logo;
    
    @FXML
    private ComboBox comboz, comboy;
    @FXML
    private BorderPane bp;

    private Label m;

    @FXML
    private ComboBox T2;

    @FXML
    private RadioButton produitr, servicer;

    @FXML
    private TextField text;
    @FXML
    private DatePicker datePicker1, datePicker2;

    @FXML
    private RangeSlider ranger;

    @FXML
    private void handleButtonAction(ActionEvent event) {
        System.out.println("You clicked me!");

    }

    @FXML
    private void servicef(ActionEvent event) {
        recherchePrix.setVisible(false);
        recherchePays.setVisible(true);
        comboz.setVisible(false);
        comboy.setVisible(true);
         tous.setOnAction(h->{selectStreamServices();if (!tous.isSelected())
                tous.setSelected(true);});
        particulier.setOnAction(h->{
            if (!particulier.isSelected())
                particulier.setSelected(true);
           
            streams1 = streams.stream().filter(f->f.getMembre().getType().equals("Particulier")).collect(Collectors.toList());
            selectStreamServices1();});
         entreprise.setOnAction(h->{
             if (!entreprise.isSelected())
                entreprise.setSelected(true);
            streams1 = streams.stream().filter(f->f.getMembre().getType().equals("Entreprise")).collect(Collectors.toList());
            selectStreamServices1();});
       
    }

    @FXML
    private void produitf(ActionEvent event) {
        recherchePrix.setVisible(true);
        recherchePays.setVisible(false);
        comboz.setVisible(true);
        comboy.setVisible(false);
       
         tous.setOnAction(h->{selectStreamProduits();
         if (!tous.isSelected())
                tous.setSelected(true);});
        particulier.setOnAction(h->{
            if (!particulier.isSelected())
                particulier.setSelected(true);
            //System.out.println(streamp.get(1).getMembre().getType());
            streamp1 = streamp.stream().filter(f->f.getMembre().getType().equals("Particulier")).collect(Collectors.toList());
            selectStreamProduits1();});
         entreprise.setOnAction(h->{
             if (!entreprise.isSelected())
                entreprise.setSelected(true);
            streamp1 = streamp.stream().filter(f->f.getMembre().getType().equals("Entreprise")).collect(Collectors.toList());
            selectStreamProduits1();});
    }

    @FXML
    private void miniTunisie(ActionEvent event) {
        c = this;
        System.out.println(c.marker.getWidth());
        vue.display(Accueil.a, T2);
        System.out.println(T2.getValue());

    }

    @FXML
    private void profil(ActionEvent event) {

        vue.getAccueil().getStage().getScene().getRoot().setEffect(new GaussianBlur());
        try {
            runAnotherApp(JavaFXApplication3.class);
        } catch (Exception ex) {
            Logger.getLogger(AccueilController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void panier(ActionEvent event) {

        try {
            runAnotherApp(Panier.class);
        } catch (Exception ex) {
            Logger.getLogger(AccueilController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void accueilA(ActionEvent event) {

        try {
            Parent root = FXMLLoader.load(getClass().getResource("Accueil0.fxml"));
            Image[] images = new Image[14];

            for (int i = 0; i < 14; i++) {
                images[i] = new Image(Shelf.class.getResource("/displayshelf/animal" + (i + 1) + ".jpg").toExternalForm(), false);
            }

            VBox vbox = new VBox();
            // create display shelf
            Shelf displayShelf = new Shelf(images);
            displayShelf.setPrefSize(300, 500);
            vbox.getChildren().addAll(root, displayShelf);
            bp.setLeft(null);
            bp.setCenter(vbox);

        } catch (IOException ex) {
            Logger.getLogger(AccueilController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    

    @FXML
    private void accueil(ActionEvent event) {

        try {
            BorderPane root = FXMLLoader.load(getClass().getResource("Accueil.fxml"));

            Accueil.scene = new Scene(root);
            scene.getStylesheets().add(getClass().getResource("/edu/esprit/pidev/technique/css/Accueil.css").toExternalForm());
            Accueil.stage.setScene(Accueil.scene);
        } catch (IOException ex) {
            Logger.getLogger(AccueilController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void rechercher(ActionEvent event) {
        tous.setSelected(true);
    Date date1= new Date();
    Date date2= new Date();
    
        if (datePicker1.getValue() != null) {
            LocalDate localDate1 = datePicker1.getValue();
            Instant instant1 = Instant.from(localDate1.atStartOfDay(ZoneId.systemDefault()));
             date1 = Date.from(instant1);
        }
 
        if (datePicker2.getValue() != null) {
            LocalDate localDate2 = datePicker2.getValue();

            Instant instant2 = Instant.from(localDate2.atStartOfDay(ZoneId.systemDefault()));
            date2 = Date.from(instant2);

            System.out.println(localDate2 + "\n" + instant2 + "\n" + date2);
        }
final Date date11= date1;
    final Date date22= date2;
        if (produitr.isSelected()) {

            streamp = produits.stream().filter(e -> e.getLibelle().contains(text.getText())).filter(e -> {
                if (comboz.getSelectionModel().getSelectedItem() == null) {
                    return true;
                } else {

                    return ((CategorieProduit) comboz.getSelectionModel().getSelectedItem()).getLibelle().equals(e.getCategorie().getLibelle());
                }
            }).filter(e -> e.getPrix() > ranger.getLowValue()).filter(e -> e.getPrix() < ranger.getHighValue()).filter(e->{Date d = new Date(e.getDate().getTime());
                
                if (datePicker1.getValue()==null) return true; else return (d.after(date11));
                
                
            
            }).filter(e->{Date d = new Date(e.getDate().getTime());
                
                if (datePicker2.getValue()==null) return true; else return (d.before(date22));
                
                
            
            }).collect(Collectors.toList());
            selectStreamProduits();
        }
        if (servicer.isSelected()) {
            streams = services.stream().filter(e -> e.getLibelle().contains(text.getText())).filter(e->{Date d = new Date(e.getDate().getTime());
                
                if (datePicker1.getValue()==null) return true; else return (d.after(date11));
                
                
            
            }).filter(e->{Date d = new Date(e.getDate().getTime());
                
                if (datePicker2.getValue()==null) return true; else return (d.before(date22));
                
                
            
            }).filter(e -> {
                if (comboy.getSelectionModel().getSelectedItem() == null) {
                    return true;
                } else {

                    return ((CategorieService) comboy.getSelectionModel().getSelectedItem()).getLibelle().equals(e.getCategorieService().getLibelle());
                }
            }).filter(e->{
            
            if (T2.getSelectionModel().getSelectedItem()== null)
             return true; else return ((String)T2.getSelectionModel().getSelectedItem()).equals(e.getGouvernorat());
            }).collect(Collectors.toList());
            selectStreamServices();
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        vue = new Pays(Accueil.a, T2);

        produitDao = new ProduitDao();
        serviceDao = new ServiceDao();
        categorieServiceDao = new CategorieServiceDao();
        categorieProduitDao = new CategorieProduitDao();
        serviceDao = new ServiceDao();
        produits = new ArrayList<>();
        services = new ArrayList<>();
        streams = new ArrayList<>();
        streamp = new ArrayList<>();
        streams1 = new ArrayList<>();
        streamp1 = new ArrayList<>();
        produits = produitDao.selectAll();
        services = serviceDao.selectAll();
        streamp = produitDao.selectAll();
        streams = serviceDao.selectAll();
        
        
        selectProduits();
        tous.setSelected(true);
        RemplirCategorieProduitCombo(comboz);
        RemplirCategorieServiceCombo(comboy);
        
        ImageView logov = new ImageView();
        logov.setImage(new Image(new File("C:\\Users\\Asus\\Documents\\NetBeansProjects\\AllForDealv3\\src\\edu\\esprit\\pidev\\images\\71.png").toURI().toString()));
        
        tous.setOnAction(h->{selectStreamProduits();if (!tous.isSelected())
                tous.setSelected(true);});
        particulier.setOnAction(h->{
            if (!particulier.isSelected())
                particulier.setSelected(true);
            streamp1 = streamp.stream().filter(f->f.getMembre().getType().equals("Particulier")).collect(Collectors.toList());
            selectStreamProduits1();});
         entreprise.setOnAction(h->{
             if (!entreprise.isSelected())
                entreprise.setSelected(true);
            streamp1 = streamp.stream().filter(f->f.getMembre().getType().equals("Entreprise")).collect(Collectors.toList());
            selectStreamProduits1();});
        
          maxt.setOnKeyReleased(x->{
            
            try{ranger.setHighValue(Integer.parseInt(maxt.getText()));}
            catch (Exception ex){
                System.out.println("erreur de conversion");
            }
            
            });
            
             mint.setOnKeyReleased(x->{
            
            try{ranger.setLowValue(Integer.parseInt(mint.getText()));}
            catch (Exception ex){
                System.out.println("erreur de conversion");
            }
            
            });
        T2.valueProperty().addListener((e, v1, v2) -> {
            marker.setVisible(true);
            
          
            switch ((String) v2) {

                case "Ariana":
                    marker.relocate(191, 9);

                    break;
                case "Beja":
                    marker.relocate(182, 12);
                    break;
                case "Ben Arous":
                    marker.relocate(195, 15);
                    break;
                case "Bizerte":
                    marker.relocate(185, 6);
                    break;
                case "Gabes":
                    marker.relocate(182, 63);
                    break;
                case "Gafsa":
                    marker.relocate(166, 55);
                    break;
                case "Jendouba":
                    marker.relocate(174, 12);
                    break;
                case "Kairouan":
                    marker.relocate(188, 30);
                    break;
                case "Kasserine":
                    marker.relocate(171, 40);
                    break;
                case "Kebili":
                    marker.relocate(166, 77);
                    break;
                case "Le Kef":
                    marker.relocate(171, 22);
                    break;
                case "Mahdia":
                    marker.relocate(205, 34);
                    ;
                    break;
                case "La Manouba":
                    marker.relocate(189, 13);
                    break;
                case "Médenine":
                    marker.relocate(190, 75);
                    break;
                case "Monastir":
                    marker.relocate(202, 30);
                    break;
                case "Nabeul":
                    marker.relocate(202, 12);
                    break;
                case "Sfax":
                    marker.relocate(197, 48);
                    break;
                case "Sidi Bouzid":
                    marker.relocate(182, 44);
                    break;
                case "Siliana":
                    marker.relocate(183, 22);
                    break;
                case "Sousse":
                    marker.relocate(198, 25);
                    break;
                case "Tataouine":
                    marker.relocate(188, 100);
                    break;
                case "Tozeur":
                    marker.relocate(154, 65);
                    break;
                case "Tunis":
                    marker.relocate(195, 12);
                    break;
                case "Zaghouan":
                    marker.relocate(191, 18);
                    break;

            }
        });
    }

    public void selectProduits() {

        vbox.getChildren().clear();
        for (Produit p : produits) {
            createProduitAnnonce(p);
        }

    }

    public void selectStreamProduits() {

        vbox.getChildren().clear();
        for (Produit p : streamp) {
            createProduitAnnonce(p);
        }

    }
      public void selectStreamProduits1() {

        vbox.getChildren().clear();
        for (Produit p : streamp1) {
            createProduitAnnonce(p);
        }

    }
    

    public void selectServices() {

        vbox.getChildren().clear();
        for (Service s : services) {
            createServiceAnnonce(s);
        }

    }

    public void selectStreamServices() {

        vbox.getChildren().clear();
        for (Service s : streams) {
            createServiceAnnonce(s);
        }

    }
    public void selectStreamServices1() {

        vbox.getChildren().clear();
        for (Service s : streams1) {
            createServiceAnnonce(s);
        }

    }

    public void createProduitAnnonce(Produit produit) {

        Pane p = new Pane();
        Label titre = new Label(produit.getLibelle());
        Label type = new Label(produit.getMembre().getType());
        ImageView im = new ImageView();
        im.setImage(new Image(new File(produit.getImage()).toURI().toString()));
        Label prix = new Label("" + produit.getPrix());
        Label date = new Label("" + produit.getDate());
        Label categorie = new Label(produit.getCategorie().getLibelle() + " ,Type");
        Button panier = new Button("Ajouter au Panier");
        panier.setOnAction(e -> {
           edu.esprit.pidev.entities.Panier paniers = new edu.esprit.pidev.entities.Panier(new Membre(1));
           paniers.ajouterProduitP(produit, 1);
            PanierDao pdao= new PanierDao();
           pdao.add(paniers);
             Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Ajouté au panier");
            alert.setContentText("Le produit a été ajouté au panier");
            alert.showAndWait();
           
        });
        p.getChildren().addAll(titre, prix, date, categorie, im, panier,type);

        im.setFitHeight(70);
        im.setFitWidth(70);

        titre.relocate(120, 10);
        im.relocate(20, 10);
        categorie.relocate(120, 30);
        prix.relocate(120, 70);
        date.relocate(420, 10);
        panier.relocate(380, 50);
        type.relocate(420, 90);
        p.setOnMouseClicked(e -> {
            try {
                ProduitController.produit = produit;
                Parent root = FXMLLoader.load(getClass().getResource("produit.fxml"));
                Stage stage = new Stage();
                stage.setTitle("Produit");
                stage.setScene(new Scene(root));
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.show();

            } catch (IOException ex) {
                ex.printStackTrace();
            }

        });
        p.setPrefSize(580, 120);
        p.setOnMouseEntered(e -> p.setEffect(new Glow()));
        p.setOnMouseExited(e -> p.setEffect(null));

        p.setStyle("  -fx-background-color: \n"
                + "        linear-gradient(#FFFFFF, #d6d6d6),\n"
                + "        linear-gradient(#FFFFFF 0%, #d9d9d9 20%, #d6d6d6 100%),\n"
                + "        linear-gradient(#FFFFFF 0%, #f6f6f6 50%);\n"
                + "    -fx-background-radius: 8,7,6;\n"
                + "    -fx-background-insets: 0,1,2;\n"
                + "    -fx-text-fill: black;\n"
                + "    -fx-effect: dropshadow( three-pass-box , rgba(0,0,0,0.6) , 5, 0.0 , 0 , 1 );");

        vbox.getChildren().add(p);

    }

    public void createServiceAnnonce(Service service) {

        Pane p = new Pane();
        Label titre = new Label(service.getLibelle());
         Label type = new Label(service.getMembre().getType());
        ImageView im = new ImageView();
        Label gouv = new Label(service.getGouvernorat());
        im.setImage(new Image(new File("C:\\Users\\Asus\\Documents\\NetBeansProjects\\AllForDealv3\\src\\edu\\esprit\\pidev\\images\\service.png").toURI().toString()));
        Label date = new Label("" + service.getDate());
        Label categorie = new Label(service.getCategorieService().getLibelle());
        Button panier = new Button("Envoyer Message");
        panier.setOnAction(e -> {
            System.out.println("ok");
        });
        p.getChildren().addAll(titre, date, categorie, im, panier, gouv,type);

        im.setFitHeight(70);
        im.setFitWidth(70);
        gouv.relocate(120, 50);
        titre.relocate(120, 10);
        im.relocate(20, 10);
        categorie.relocate(120, 30);

        date.relocate(420, 10);
        panier.relocate(380, 50);
        type.relocate(420, 90);
        p.setOnMouseClicked(e -> {
            try {
                   ServiceController.service = service;
                Parent root = FXMLLoader.load(getClass().getResource("service.fxml"));
                Stage stage = new Stage();
                stage.setTitle("Service");
                stage.setScene(new Scene(root));
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.show();

            } catch (IOException ex) {
                ex.printStackTrace();
            }

        });
        p.setPrefSize(580, 120);
        p.setOnMouseEntered(e -> p.setEffect(new Glow()));
        p.setOnMouseExited(e -> p.setEffect(null));

        p.setStyle("  -fx-background-color: \n"
                + "        linear-gradient(#FFFFFF, #d6d6d6),\n"
                + "        linear-gradient(#FFFFFF 0%, #d9d9d9 20%, #d6d6d6 100%),\n"
                + "        linear-gradient(#FFFFFF 0%, #f6f6f6 50%);\n"
                + "    -fx-background-radius: 8,7,6;\n"
                + "    -fx-background-insets: 0,1,2;\n"
                + "    -fx-text-fill: black;\n"
                + "    -fx-effect: dropshadow( three-pass-box , rgba(0,0,0,0.6) , 5, 0.0 , 0 , 1 );");

        vbox.getChildren().add(p);

    }

    public void RemplirCategorieServiceCombo(ComboBox combo) {
        ObservableList<CategorieService> data1 = FXCollections.observableArrayList(categorieServiceDao.selectAll());
        combo.setCellFactory(
                new Callback<ListView<CategorieService>, ListCell<CategorieService>>() {
                    @Override
                    public ListCell<CategorieService> call(ListView<CategorieService> param) {
                        final ListCell<CategorieService> cell = new ListCell<CategorieService>() {

                            @Override
                            public void updateItem(CategorieService item,
                                    boolean empty) {
                                super.updateItem(item, empty);
                                if (item != null) {
                                    setText(item.getLibelle());
                                } else {
                                    setText(null);
                                }
                            }
                        };
                        return cell;
                    }
                });
        combo.setButtonCell(new ListCell<CategorieService>() {
            @Override
            protected void updateItem(CategorieService p, boolean bln) {
                super.updateItem(p, bln);
                if (p != null) {
                    setText(p.getLibelle());
                } else {
                    setText(null);
                }
            }
        });
        combo.setItems(data1);
    }

  
    public void RemplirCategorieProduitCombo(ComboBox combo) {
        ObservableList<CategorieProduit> data1 = FXCollections.observableArrayList(categorieProduitDao.selectAll());
        combo.setCellFactory(
                new Callback<ListView<CategorieProduit>, ListCell<CategorieProduit>>() {
                    @Override
                    public ListCell<CategorieProduit> call(ListView<CategorieProduit> param) {
                        final ListCell<CategorieProduit> cell = new ListCell<CategorieProduit>() {

                            @Override
                            public void updateItem(CategorieProduit item,
                                    boolean empty) {
                                super.updateItem(item, empty);
                                if (item != null) {
                                    setText(item.getLibelle());
                                } else {
                                    setText(null);
                                }
                            }
                        };
                        return cell;
                    }
                });
        combo.setButtonCell(new ListCell<CategorieProduit>() {
            @Override
            protected void updateItem(CategorieProduit p, boolean bln) {
                super.updateItem(p, bln);
                if (p != null) {
                    setText(p.getLibelle());
                } else {
                    setText(null);
                }
            }
        });
        combo.setItems(data1);
    }

    public void runAnotherApp(Class<? extends Application> anotherAppClass) throws Exception {
        Application app2 = anotherAppClass.newInstance();
        Stage anotherStage = new Stage();
        app2.start(anotherStage);
    }

    public Label getMarker() {
        return c.marker;
    }

}
