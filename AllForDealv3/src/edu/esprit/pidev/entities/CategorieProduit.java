/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.esprit.pidev.entities;

/**
 *
 * @author Asus
 */
public class CategorieProduit {
    
    private int id;
    private String libelle;
    

    public CategorieProduit(int id, String libelle) {
        this.id = id;
        this.libelle = libelle;
    }
    public CategorieProduit(String libelle) {
        
        this.libelle = libelle;
    }
    public CategorieProduit() {
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @Override
    public String toString() {
        return "CategorieProduit{" + "id=" + id + ", libelle=" + libelle + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CategorieProduit other = (CategorieProduit) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
    
    
    
    
    
}
