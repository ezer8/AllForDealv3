/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.esprit.pidev.entities;

import edu.esprit.pidev.dao.classes.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author GhassenNasri
 */
public class Panier {

    private Membre membre;
    private Map<Produit, Integer> produitsPanier;
    private float total;

    public Panier() {
         produitsPanier = new HashMap<>();
    }

    public Panier(Membre membre) {
        produitsPanier = new HashMap<>();
        this.membre = membre;
    }

    public Membre getMembre() {
        return membre;
    }

    public void setMembre(Membre membre) {
        this.membre = membre;
    }

    public Map<Produit, Integer> getProduitsPanier() {
        return produitsPanier;
    }

    public float getTotal() {
        return total;
    }

    public void setProduitsPanier(Map<Produit, Integer> produitsPanier) {
        this.produitsPanier = produitsPanier;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public void ajouterProduitP(Produit produit, int quantite) {
        if (produitsPanier.containsKey(produit)) {
            produitsPanier.put(produit, (produitsPanier.get(produit) + quantite));
       } else {
            produitsPanier.put(produit, quantite);
        }
    }

    public void deleteProduitP(Produit produit) {
        produitsPanier.remove(produit);
    }

    public void viderPanier() {
        produitsPanier.clear();
    }

    public void afficher() {
        System.out.println(produitsPanier.keySet());
        System.out.println(produitsPanier.values());
    }

    public int modifQuantiteP(Produit produit, int quantite) {
        return produitsPanier.replace(produit, quantite);
    }

    public void calculTotatP() {
        float x = 0f;
        for (Produit p : produitsPanier.keySet()) {
            x = x + p.getPrix() * produitsPanier.get(p);
        }
        this.total = x;
    }

    public String getListeProduits() {
        String ch = "";
        for (Produit p : produitsPanier.keySet()) {
            ch = ch + "Produit : " + p.getLibelle() + " Code : " + p.getId() + " Quantite : " + produitsPanier.get(p) + " | ";
        }
        return ch;
    }

    public Set<Produit> getProduitsP() {

        return produitsPanier.keySet();
    }

    @Override
    public String toString() {
        return "Panier{" + "produitsPanier=" + produitsPanier + ", total=" + total + '}';
    }

}
