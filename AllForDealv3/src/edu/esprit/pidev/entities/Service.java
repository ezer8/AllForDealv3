/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.esprit.pidev.entities;


import java.sql.Date;

/**
 *
 * @author Asus
 */
public class Service {
    
    private int id;
    private String libelle;
    private Date date;
    private String description;
    private Membre membre;
    private CategorieService categorieService;
    private String gouvernorat;

    public Service() {
    }

    public Service(String libelle, Date date, String description, Membre membre, CategorieService categorieService) {
        this.libelle = libelle;
        this.date = date;
        this.description = description;
        this.membre = membre;
        this.categorieService = categorieService;
    }

 

    public Service(int id, String libelle, Date date, String description, Membre membre, CategorieService categorieService) {
        this.id = id;
        this.libelle = libelle;
        this.date = date;
        this.description = description;
        this.membre = membre;
        this.categorieService = categorieService;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Membre getMembre() {
        return membre;
    }

    public void setMembre(Membre membre) {
        this.membre = membre;
    }

    public CategorieService getCategorieService() {
        return categorieService;
    }

    public void setCategorieService(CategorieService categorieService) {
        this.categorieService = categorieService;
    }

    @Override
    public String toString() {
        return "Service{" + "id=" + id + ", libelle=" + libelle + ", date=" + date + ", description=" + description + ", membre=" + membre + ", categorieService=" + categorieService + '}';
    }

    public String getGouvernorat() {
        return gouvernorat;
    }

    public void setGouvernorat(String gouvernorat) {
        this.gouvernorat = gouvernorat;
    }

    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Service other = (Service) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    
    
    
    
}
