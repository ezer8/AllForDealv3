/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.esprit.pidev.entities;

import edu.esprit.pidev.dao.classes.*;
import java.sql.Date;

/**
 *
 * @author GhassenNasri
 */
public class Achat {

    private int id;
    private Date date;
    private Membre membre;
    private String produits;
    private float total;

    public Achat() {

    }

    public Achat(int id, Date date, Membre membre, String produits, float total) {
        this.id = id;
        this.date = date;
        this.membre = membre;
        this.produits = produits;
          this.total = total;
    }

    public int getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }

    public Membre getMembre() {
        return membre;
    }

    public String getProduits() {
        return produits;
    }

    public float getTotal() {
        return total;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setMembre(Membre membre) {
        this.membre = membre;
    }

    public void setProduits(String produits) {
        this.produits = produits;
    }

    public void setTotal(float total) {
        this.total = total;
    }

}