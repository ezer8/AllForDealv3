/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.esprit.pidev.entities;


import java.sql.Date;

/**
 *
 * @author Asus
 */
public class OffreService {
    
    private int id;
    private String libelle;
    private Date date;
    private String description;
    private Membre membre;
    private CategorieService categorieService;
    private Service service;
    private String etat;

    public OffreService() {
    }

    public OffreService(String libelle, Date date, String description, Membre membre, CategorieService categorieService, Service service, String etat) {
        this.libelle = libelle;
        this.date = date;
        this.description = description;
        this.membre = membre;
        this.categorieService = categorieService;
        this.service = service;
        this.etat = etat;
    }

    public OffreService(int id, String libelle, Date date, String description, Membre membre, CategorieService categorieService, Service service, String etat) {
        this.id = id;
        this.libelle = libelle;
        this.date = date;
        this.description = description;
        this.membre = membre;
        this.categorieService = categorieService;
        this.service = service;
        this.etat = etat;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Membre getMembre() {
        return membre;
    }

    public void setMembre(Membre membre) {
        this.membre = membre;
    }

    public CategorieService getCategorieService() {
        return categorieService;
    }

    public void setCategorieService(CategorieService categorieService) {
        this.categorieService = categorieService;
    }

    @Override
    public String toString() {
        return "OffreService{" + "id=" + id + ", libelle=" + libelle + ", date=" + date + ", description=" + description + ", membre=" + membre + ", categorieService=" + categorieService + ", service=" + service + ", etat=" + etat + '}';
    }

 

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OffreService other = (OffreService) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
    
    
    
    
}
