/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.esprit.pidev.entities;

import java.sql.Date;

/**
 *
 * @author Asus
 */
public class Evaluation {
    
    private int id;
    private int evaluation;
    private String commentaire;
    private Date date;
    private Produit produit;
    private Service service;
    private Membre membre;

    public Evaluation() {
    }

    public Evaluation(int id, int evaluation, String commentaire, Date date, Produit produits, Service services,Membre membre) {
        this.id = id;
        this.evaluation = evaluation;
        this.commentaire = commentaire;
        this.date = date;
        this.produit = produits;
        this.service = services;
        this.membre = membre;
    }
      public Evaluation( int evaluation, String commentaire, Date date, Produit produits, Service services,Membre membre) {
     
        this.evaluation = evaluation;
        this.commentaire = commentaire;
        this.date = date;
        this.produit = produits;
        this.service = services;
        this.membre = membre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(int evaluation) {
        this.evaluation = evaluation;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Produit getProduit() {
        return produit;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public Membre getMembre() {
        return membre;
    }

    public void setMembre(Membre membre) {
        this.membre = membre;
    }
    

    @Override
    public String toString() {
        return "Evaluation{" + "id=" + id + ", evaluation=" + evaluation + ", commentaire=" + commentaire + ", date=" + date + ", produit=" + produit + ", service=" + service + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Evaluation other = (Evaluation) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    
    
}
