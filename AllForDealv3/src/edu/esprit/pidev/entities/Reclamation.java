/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.esprit.pidev.entities;

import edu.esprit.pidev.dao.classes.*;
import java.sql.Date;

/**
 *
 * @author Asus
 */
public class Reclamation {
    
    private int id;
    private String titre;
    private String description;
    private Date date;
    private Membre membres;
    private String user;

    public Reclamation() {
    }

    public Reclamation(int id, String titre, String description, Date date, Membre membres) {
        this.id = id;
        this.titre = titre;
        this.description = description;
        this.date = date;
        this.membres = membres;
        user = membres.getLogin();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Membre getMembres() {
        return membres;
    }

    public void setMembres(Membre membres) {
        this.membres = membres;
        this.user = membres.getLogin();
    }

    public String getUser() {
        return user;
    }

    @Override
    public String toString() {
        return "Reclamation{" + "id=" + id + ", titre=" + titre + ", description=" + description + ", date=" + date + ", membres=" + membres + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Reclamation other = (Reclamation) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
    
    
    
}
