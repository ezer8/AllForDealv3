/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.esprit.pidev.entities;

import edu.esprit.pidev.dao.classes.*;
import java.util.Date;

/**
 *
 * @author Asus
 */
public class Membre {
    private int id;
    private String login;
    private String password;
    private String nom;
    private String prenom;
    private int age;
    private String email;
    private String numeroTel;
    private String adresse;
    private int bonus;
    private Date dateVisite;
    private String type;
    private int nbProdVendu;

    public Membre(int id) {
        this.id = id;
    }

    public Membre(String nom, String prenom, int age, String email, String numeroTel, String adresse, Date dateVisite, String type) {
        this.nom = nom;
        this.prenom = prenom;
        this.age = age;
        this.email = email;
        this.numeroTel = numeroTel;
        this.adresse = adresse;
        this.dateVisite = dateVisite;
        this.type = type;
    }
    
    

    public Membre(String login, String password, String nom, String prenom, int age, String email, String numeroTel, String adresse, int bonus, Date dVisite, String type) {
        this.login = login;
        this.password = password;
        this.nom = nom;
        this.prenom = prenom;
        this.age = age;
        this.email = email;
        this.numeroTel = numeroTel;
        this.adresse = adresse;
        this.bonus = bonus;
        this.dateVisite = dVisite;
        this.type = type;
    }
    public Membre(String login, String password, String nom, String prenom, int age, String email, String numeroTel, String adresse, int bonus, Date dVisite, String type,int nbProdVendu) {
        this.login = login;
        this.password = password;
        this.nom = nom;
        this.prenom = prenom;
        this.age = age;
        this.email = email;
        this.numeroTel = numeroTel;
        this.adresse = adresse;
        this.bonus = bonus;
        this.dateVisite = dVisite;
        this.type = type;
        this.nbProdVendu = nbProdVendu;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNumeroTel() {
        return numeroTel;
    }

    public void setNumeroTel(String numeroTel) {
        this.numeroTel = numeroTel;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public int getBonus() {
        return bonus;
    }

    public void setBonus(int bonus) {
        this.bonus = bonus;
    }

    public Date getDateVisite() {
        return dateVisite;
    }

    public void setDateVisite(Date dateVisite) {
        this.dateVisite = dateVisite;
    }

    

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getNbProdVendu() {
        return nbProdVendu;
    }

    public void setNbProdVendu(int nbProdVendu) {
        this.nbProdVendu = nbProdVendu;
    }

    @Override
    public String toString() {
        return "Membre{" + "id=" + id + ", login=" + login + ", password=" + password + ", nom=" + nom + ", prenom=" + prenom + ", age=" + age + ", email=" + email + ", numeroTel=" + numeroTel + ", adresse=" + adresse + ", bonus=" + bonus + ", dateVisite=" + dateVisite + ", type=" + type + ", nbProdVendu=" + nbProdVendu + '}';
    }

    
    
}
