/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.esprit.pidev.entities;

import edu.esprit.pidev.dao.classes.*;
import java.util.Date;

/**
 *
 * @author Asus
 */
public class Produit {

    private int id;

    private String libelle;
    private String description;
    private Date date;
    private float prix;
    private String etat;
    private String image;
    private CategorieProduit categorie;
    private String categorieCol;
    private Membre membre;
    private String user;
    private int nbVente;
    
    public Produit(int id, String libelle, String description, Date date, float prix, String etat, String image, CategorieProduit idcategorie, Membre idmembre) {
        this.id = id;
        this.libelle = libelle;
        this.description = description;
        this.date = date;
        this.prix = prix;
        this.etat = etat;
        this.image = image;
        this.categorie = idcategorie;
        this.membre = idmembre;
    }
    public Produit(int id, String libelle, String description, Date date, float prix, String etat, String image, CategorieProduit idcategorie, Membre idmembre,int nbVente) {
        this.id = id;
        this.libelle = libelle;
        this.description = description;
        this.date = date;
        this.prix = prix;
        this.etat = etat;
        this.image = image;
        this.categorie = idcategorie;
        this.membre = idmembre;
        this.nbVente = nbVente;
    }

    public Produit(int id, String libelle, float prix, String image) {
        this.id = id;
        this.libelle = libelle;
        this.prix = prix;
        this.image = image;
    }
    public Produit(){
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public CategorieProduit getCategorie() {
        return categorie;
    }

    public void setCategorie(CategorieProduit categorie) {
        this.categorie = categorie;
        categorieCol = categorie.getLibelle();
    }

    public Membre getMembre() {
        return membre;
    }

    public void setMembre(Membre membre) {
        this.membre = membre;
        user = membre.getLogin();
    }

    public String getCategorieCol() {
        return categorieCol;
    }

    

    public String getUser() {
        return user;
    }

    public int getNbVente() {
        return nbVente;
    }

    public void setNbVente(int nbVente) {
        this.nbVente = nbVente;
    }

    @Override
    public String toString() {
        return "Produit{" + "id=" + id + ", libelle=" + libelle + ", description=" + description + ", date=" + date + ", prix=" + prix + ", etat=" + etat + ", image=" + image + ", categorie=" + categorie + ", categorieCol=" + categorieCol + ", membre=" + membre + ", user=" + user + ", nbVente=" + nbVente + '}';
    }

    
    
}
    